<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="no" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="no" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="borkedlabs-passives">
<packages>
<package name="0805">
<smd name="1" x="-0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.032" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.794" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.778" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="TO220ACS">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
2-lead molded, vertical</description>
<wire x1="5.08" y1="-1.143" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-5.08" y2="-1.143" width="0.1524" layer="21"/>
<circle x="-4.4958" y="-3.7084" radius="0.254" width="0" layer="21"/>
<pad name="C" x="-2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="A" x="2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-6.0452" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-0.762" x2="5.334" y2="0" layer="21"/>
<rectangle x1="-5.334" y1="-1.27" x2="-3.429" y2="-0.762" layer="21"/>
<rectangle x1="-3.429" y1="-1.27" x2="-1.651" y2="-0.762" layer="51"/>
<rectangle x1="3.429" y1="-1.27" x2="5.334" y2="-0.762" layer="21"/>
<rectangle x1="1.651" y1="-1.27" x2="3.429" y2="-0.762" layer="51"/>
<rectangle x1="-1.651" y1="-1.27" x2="1.651" y2="-0.762" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<text x="-3.81" y="-6.858" size="1.27" layer="97">&gt;PRECISION</text>
<text x="-3.81" y="-5.08" size="1.27" layer="97">&gt;PACKAGE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2010"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0402-RES" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0402"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2512"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TO220ACS" package="TO220ACS">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="lights">
<packages>
<package name="LED1206FAB">
<description>LED1206 FAB style (smaller pads to allow trace between)</description>
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<text x="-1.778" y="1.27" size="1.016" layer="25" ratio="15">&gt;NAME</text>
<text x="-1.778" y="-2.286" size="1.016" layer="27" ratio="15">&gt;VALUE</text>
</package>
<package name="5MM">
<description>5mm round through hole part.</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="IN" x="-1.27" y="0" drill="0.8128" diameter="1.4224"/>
<pad name="OUT" x="1.27" y="0" drill="0.8128" diameter="1.4224"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED0805">
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-0.0778" y1="0.2818" x2="0.1278" y2="0" width="0.127" layer="21"/>
<wire x1="0.1278" y1="0" x2="-0.0778" y2="-0.2818" width="0.127" layer="21"/>
<wire x1="-0.0778" y1="0.2818" x2="-0.0778" y2="-0.2818" width="0.127" layer="21"/>
</package>
<package name="LED1206">
<description>LED 1206 pads (standard pattern)</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<description>LED</description>
<wire x1="1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.778" x2="-3.429" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-3.302" y2="-0.762" width="0.1524" layer="94"/>
<text x="3.556" y="-2.032" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-2.032" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="0.381"/>
<vertex x="-3.048" y="1.27"/>
<vertex x="-2.54" y="0.762"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-0.762"/>
<vertex x="-2.921" y="0.127"/>
<vertex x="-2.413" y="-0.381"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="D">
<description>LED</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED1206">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FAB1206" package="LED1206FAB">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="5MM">
<connects>
<connect gate="G$1" pin="A" pad="IN"/>
<connect gate="G$1" pin="C" pad="OUT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="LED0805">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="fab">
<packages>
<package name="CBA-SILK-LOGO">
<circle x="0" y="0" radius="0.254" width="0.127" layer="21"/>
<circle x="-0.762" y="0.762" radius="0.254" width="0.127" layer="21"/>
<wire x1="-0.254" y1="1.016" x2="0.254" y2="1.016" width="0.127" layer="21"/>
<wire x1="0.254" y1="1.016" x2="0.254" y2="0.508" width="0.127" layer="21"/>
<wire x1="0.254" y1="0.508" x2="-0.254" y2="0.508" width="0.127" layer="21"/>
<wire x1="-0.254" y1="0.508" x2="-0.254" y2="1.016" width="0.127" layer="21"/>
<wire x1="-1.016" y1="0.254" x2="-0.508" y2="0.254" width="0.127" layer="21"/>
<wire x1="-0.508" y1="0.254" x2="-0.508" y2="-0.254" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-0.254" x2="-1.016" y2="-0.254" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-0.254" x2="-1.016" y2="0.254" width="0.127" layer="21"/>
<wire x1="0.508" y1="0.508" x2="1.016" y2="0.508" width="0.127" layer="21"/>
<wire x1="1.016" y1="0.508" x2="1.016" y2="1.016" width="0.127" layer="21"/>
<wire x1="1.016" y1="1.016" x2="0.508" y2="1.016" width="0.127" layer="21"/>
<wire x1="0.508" y1="1.016" x2="0.508" y2="0.508" width="0.127" layer="21"/>
<wire x1="0.508" y1="0.254" x2="1.016" y2="0.254" width="0.127" layer="21"/>
<wire x1="1.016" y1="0.254" x2="1.016" y2="-0.254" width="0.127" layer="21"/>
<wire x1="1.016" y1="-0.254" x2="0.508" y2="-0.254" width="0.127" layer="21"/>
<wire x1="0.508" y1="-0.254" x2="0.508" y2="0.254" width="0.127" layer="21"/>
<wire x1="0.508" y1="-0.508" x2="1.016" y2="-0.508" width="0.127" layer="21"/>
<wire x1="1.016" y1="-0.508" x2="1.016" y2="-1.016" width="0.127" layer="21"/>
<wire x1="1.016" y1="-1.016" x2="0.508" y2="-1.016" width="0.127" layer="21"/>
<wire x1="0.508" y1="-1.016" x2="0.508" y2="-0.508" width="0.127" layer="21"/>
<wire x1="0.254" y1="-0.508" x2="-0.254" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-0.508" x2="-0.254" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-1.016" x2="0.254" y2="-1.016" width="0.127" layer="21"/>
<wire x1="0.254" y1="-1.016" x2="0.254" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-0.508" x2="-1.016" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-0.508" x2="-1.016" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-1.016" x2="-0.508" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-1.016" x2="-0.508" y2="-0.508" width="0.127" layer="21"/>
</package>
<package name="MK-LOGO-SILK">
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<wire x1="-0.9525" y1="-1.016" x2="-0.9525" y2="1.016" width="0.127" layer="21"/>
<wire x1="-0.5715" y1="0" x2="-0.9525" y2="1.016" width="0.127" layer="21"/>
<wire x1="-0.1905" y1="1.016" x2="-0.1905" y2="-1.016" width="0.127" layer="21"/>
<wire x1="0.1905" y1="-1.016" x2="0.1905" y2="0" width="0.127" layer="21"/>
<wire x1="0.1905" y1="0" x2="0.1905" y2="1.016" width="0.127" layer="21"/>
<wire x1="0.1905" y1="0" x2="0.9525" y2="1.016" width="0.127" layer="21"/>
<wire x1="0.1905" y1="0" x2="0.9525" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.5715" y1="0" x2="-0.1905" y2="1.016" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
</symbols>
<devicesets>
<deviceset name="CBA-LOGO">
<gates>
</gates>
<devices>
<device name="" package="CBA-SILK-LOGO">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MK-LOGO">
<gates>
</gates>
<devices>
<device name="" package="MK-LOGO-SILK">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:26929/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" urn="urn:adsk.eagle:component:26963/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="power">
<packages>
<package name="SOT23-5">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;, 5 lead</description>
<wire x1="-1.544" y1="0.713" x2="1.544" y2="0.713" width="0.1524" layer="51"/>
<wire x1="1.544" y1="0.713" x2="1.544" y2="-0.712" width="0.1524" layer="51"/>
<wire x1="1.544" y1="-0.712" x2="-1.544" y2="-0.712" width="0.1524" layer="51"/>
<wire x1="-1.544" y1="-0.712" x2="-1.544" y2="0.713" width="0.1524" layer="51"/>
<smd name="5" x="-0.95" y="1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="4" x="0.95" y="1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="1" x="-0.95" y="-1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="2" x="0" y="-1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="3" x="0.95" y="-1.306" dx="0.5334" dy="1.1938" layer="1"/>
<text x="-1.778" y="-1.778" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.048" y="-1.778" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.1875" y1="0.7126" x2="-0.7125" y2="1.5439" layer="51"/>
<rectangle x1="0.7125" y1="0.7126" x2="1.1875" y2="1.5439" layer="51"/>
<rectangle x1="-1.1875" y1="-1.5437" x2="-0.7125" y2="-0.7124" layer="51"/>
<rectangle x1="-0.2375" y1="-1.5437" x2="0.2375" y2="-0.7124" layer="51"/>
<rectangle x1="0.7125" y1="-1.5437" x2="1.1875" y2="-0.7124" layer="51"/>
<wire x1="-1.5" y1="-1.9" x2="-1.5" y2="-1.2" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="VREG-AP2112">
<pin name="VIN" x="-12.7" y="2.54" length="middle"/>
<pin name="EN" x="-12.7" y="-2.54" length="middle"/>
<pin name="GND" x="0" y="-10.16" length="middle" rot="R90"/>
<pin name="VOUT" x="12.7" y="2.54" length="middle" rot="R180"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-2.54" y="7.62" size="1.27" layer="95">&gt;NAME</text>
<text x="2.54" y="-7.62" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="VREG-AP2112" prefix="U">
<gates>
<gate name="G$1" symbol="VREG-AP2112" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-5">
<connects>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VIN" pad="1"/>
<connect gate="G$1" pin="VOUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="microcontrollers">
<packages>
<package name="QFN-64-9X9MM-SMALLPAD">
<description>&lt;h3&gt;64-pin QFN 9x9mm, 0.5mm pitch&lt;/h3&gt;
&lt;p&gt;Package used by ATmega128RFA1&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.atmel.com/Images/Atmel-8266-MCU_Wireless-ATmega128RFA1_Datasheet.pdf"&gt;Example Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-4.492" y1="-4.5" x2="4.508" y2="-4.5" width="0.09" layer="51"/>
<wire x1="4.508" y1="-4.5" x2="4.508" y2="4.5" width="0.09" layer="51"/>
<wire x1="4.508" y1="4.5" x2="-4.492" y2="4.5" width="0.09" layer="51"/>
<wire x1="-4.492" y1="4.5" x2="-4.492" y2="-4.5" width="0.09" layer="51"/>
<wire x1="-4.6" y1="4.6" x2="-4.6" y2="4.1" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="4.6" x2="-4.1" y2="4.6" width="0.2032" layer="21"/>
<wire x1="4.6" y1="4.6" x2="4.1" y2="4.6" width="0.2032" layer="21"/>
<wire x1="4.6" y1="4.6" x2="4.6" y2="4.1" width="0.2032" layer="21"/>
<circle x="-4.842" y="4.85" radius="0.2" width="0" layer="21"/>
<circle x="-3.442" y="3.45" radius="0.2" width="0.09" layer="51"/>
<smd name="26" x="0.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="25" x="0.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="24" x="-0.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="27" x="1.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="28" x="1.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="23" x="-0.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="22" x="-1.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="21" x="-1.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="6" x="-4.5" y="1.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="5" x="-4.5" y="1.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="4" x="-4.5" y="2.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="7" x="-4.5" y="0.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="8" x="-4.5" y="0.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="3" x="-4.5" y="2.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="2" x="-4.5" y="3.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="9" x="-4.5" y="-0.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="10" x="-4.5" y="-0.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="1" x="-4.5" y="3.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="16" x="-4.5" y="-3.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="15" x="-4.5" y="-3.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="14" x="-4.5" y="-2.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="17" x="-3.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="18" x="-3.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="13" x="-4.5" y="-2.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="12" x="-4.5" y="-1.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="19" x="-2.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="20" x="-2.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="11" x="-4.5" y="-1.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="29" x="2.25" y="-4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="30" x="2.75" y="-4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="31" x="3.25" y="-4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="32" x="3.75" y="-4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="33" x="4.5" y="-3.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="34" x="4.5" y="-3.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="35" x="4.5" y="-2.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="36" x="4.5" y="-2.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="37" x="4.5" y="-1.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="38" x="4.5" y="-1.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="39" x="4.5" y="-0.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="40" x="4.5" y="-0.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="41" x="4.5" y="0.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="42" x="4.5" y="0.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="43" x="4.5" y="1.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="44" x="4.5" y="1.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="45" x="4.5" y="2.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="46" x="4.5" y="2.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="47" x="4.5" y="3.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="48" x="4.5" y="3.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="49" x="3.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="50" x="3.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="51" x="2.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="52" x="2.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="53" x="1.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="54" x="1.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="55" x="0.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="56" x="0.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="57" x="-0.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="58" x="-0.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="59" x="-1.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="60" x="-1.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="61" x="-2.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="62" x="-2.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="63" x="-3.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="64" x="-3.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<text x="0" y="1.27" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="4.6" y1="-4.6" x2="4.1" y2="-4.6" width="0.2032" layer="21"/>
<wire x1="4.6" y1="-4.6" x2="4.6" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="-4.6" x2="-4.6" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="-4.6" x2="-4.1" y2="-4.6" width="0.2032" layer="21"/>
<smd name="P$1" x="0" y="0" dx="4.8" dy="4.8" layer="1" cream="no"/>
<polygon width="0.127" layer="31">
<vertex x="1.03" y="1.03"/>
<vertex x="1.03" y="2.17"/>
<vertex x="2.17" y="2.17"/>
<vertex x="2.17" y="1.03"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.17" y="1.03"/>
<vertex x="-2.17" y="2.17"/>
<vertex x="-1.03" y="2.17"/>
<vertex x="-1.03" y="1.03"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.17" y="-2.17"/>
<vertex x="-2.17" y="-1.03"/>
<vertex x="-1.03" y="-1.03"/>
<vertex x="-1.03" y="-2.17"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="1.03" y="-2.17"/>
<vertex x="1.03" y="-1.03"/>
<vertex x="2.17" y="-1.03"/>
<vertex x="2.17" y="-2.17"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-0.57" y="-0.57"/>
<vertex x="-0.57" y="0.57"/>
<vertex x="0.57" y="0.57"/>
<vertex x="0.57" y="-0.57"/>
</polygon>
</package>
<package name="TC2030-MCP">
<description>&lt;b&gt;TAG-CONNECT ICSP Connector&lt;/b&gt; - Legged version&lt;BR&gt;&lt;I&gt;Manufacturer:&lt;/I&gt; &lt;a href="http://www.tag-connect.com"&gt;Tag-Connect&lt;/a&gt;
&lt;p&gt;
Cable for easy In-Circuit Serial Programming. Designed for Microchip ICD2, suitable for many others.
&lt;p&gt;
&lt;b&gt;NOTE:&lt;/b&gt; Eagle's default spacing for drill holes does not leave sufficent room for routing traces for this footprint and should be adjusted. &lt;br&gt;
This setting can be found in the board layout editor under the Edit menu.  Select "Design Rules" and then the Distance tab.  8 mils for Drill/Hole works well.
&lt;br&gt;
&lt;TABLE cellspacing=0 cellpadding=0 border=0&gt;
&lt;TR&gt;&lt;TD width=20&gt;&lt;/TD&gt;&lt;TD&gt;
&lt;TABLE cellspacing=0 cellpadding=1 border=1&gt;
&lt;TR bgcolor=silver&gt;&lt;TD align=center&gt;PAD&lt;/TD&gt;&lt;TD align=center&gt;Description&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;1&lt;/TD&gt;&lt;TD&gt;MCLR/Vpp&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;2&lt;/TD&gt;&lt;TD&gt;Vdd&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;3&lt;/TD&gt;&lt;TD&gt;GND&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;4&lt;/TD&gt;&lt;TD&gt;PGD (ISPDAT)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;5&lt;/TD&gt;&lt;TD&gt;PGC (ISPCLK)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;6&lt;/TD&gt;&lt;TD&gt;nc (used for LVP)&lt;/TD&gt;&lt;/TR&gt;
&lt;/TABLE&gt;
&lt;/TD&gt;&lt;/TR&gt;&lt;/TABLE&gt;&lt;BR&gt;&lt;BR&gt;
©2009 ROFA.cz - modified and updated by Robert Darlington &amp;#8249;rdarlington@gmail.com&amp;#8250;</description>
<smd name="2" x="1.27" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="4" x="2.54" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="6" x="3.81" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="5" x="3.81" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="3" x="2.54" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="1" x="1.27" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<text x="-1.27" y="-2.54" size="1.27" layer="25" rot="R90">&gt;name</text>
<hole x="0" y="0" drill="0.889"/>
<hole x="5.08" y="-1.016" drill="0.889"/>
<hole x="5.08" y="1.016" drill="0.889"/>
<hole x="0" y="2.54" drill="2.3748"/>
<hole x="0" y="-2.54" drill="2.3748"/>
<hole x="3.175" y="-2.54" drill="2.3748"/>
<hole x="3.175" y="2.54" drill="2.3748"/>
<polygon width="0.0254" layer="39">
<vertex x="1.27" y="-0.2413"/>
<vertex x="1.6637" y="-0.2413"/>
<vertex x="1.6637" y="-0.635"/>
<vertex x="2.1463" y="-0.635"/>
<vertex x="2.1463" y="-0.2413"/>
<vertex x="2.9337" y="-0.2413"/>
<vertex x="2.9337" y="-0.635"/>
<vertex x="3.4163" y="-0.635"/>
<vertex x="3.4163" y="-0.2413"/>
<vertex x="3.81" y="-0.2413"/>
<vertex x="3.81" y="0.2413"/>
<vertex x="3.4163" y="0.2413"/>
<vertex x="3.4163" y="0.635"/>
<vertex x="2.9337" y="0.635"/>
<vertex x="2.9337" y="0.2413"/>
<vertex x="2.1463" y="0.2413"/>
<vertex x="2.1463" y="0.635"/>
<vertex x="1.6637" y="0.635"/>
<vertex x="1.6637" y="0.2413"/>
<vertex x="1.27" y="0.2413"/>
</polygon>
</package>
<package name="TC2030-MCP-NL">
<description>&lt;B&gt;TAG-CONNECT ICSP Connector&lt;/B&gt;&lt;BR&gt;&lt;I&gt;Manufacturer:&lt;/I&gt; &lt;a href="www.tag-connect.com"&gt;Tag-Connect&lt;/a&gt;&lt;BR&gt;
&lt;BR&gt;Cable for easy In-Circuit Serial Programming. Designed for Microchip ICD2, suitable for many others.&lt;BR&gt;

&lt;TABLE cellspacing=0 cellpadding=0 border=0&gt;
&lt;TR&gt;&lt;TD width=20&gt;&lt;/TD&gt;&lt;TD&gt;
&lt;TABLE cellspacing=0 cellpadding=1 border=1&gt;
&lt;TR bgcolor=silver&gt;&lt;TD align=center&gt;PAD&lt;/TD&gt;&lt;TD align=center&gt;Description&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;1&lt;/TD&gt;&lt;TD&gt;MCLR/Vpp&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;2&lt;/TD&gt;&lt;TD&gt;Vdd&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;3&lt;/TD&gt;&lt;TD&gt;GND&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;4&lt;/TD&gt;&lt;TD&gt;PGD (ISPDAT)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;5&lt;/TD&gt;&lt;TD&gt;PGC (ISPCLK)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;6&lt;/TD&gt;&lt;TD&gt;nc (used for LVP)&lt;/TD&gt;&lt;/TR&gt;
&lt;/TABLE&gt;
&lt;/TD&gt;&lt;/TR&gt;&lt;/TABLE&gt;&lt;BR&gt;&lt;BR&gt;

©2009 ROFA.cz - modified and updated by Robert Darlington &amp;#8249;rdarlington@gmail.com&amp;#8250;</description>
<smd name="2" x="1.27" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="4" x="2.54" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="6" x="3.81" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="5" x="3.81" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="3" x="2.54" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="1" x="1.27" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<hole x="0" y="0" drill="0.889"/>
<hole x="5.08" y="-1.016" drill="0.889"/>
<hole x="5.08" y="1.016" drill="0.889"/>
<polygon width="0.0254" layer="39">
<vertex x="1.27" y="-0.2413"/>
<vertex x="1.6637" y="-0.2413"/>
<vertex x="1.6637" y="-0.635"/>
<vertex x="2.1463" y="-0.635"/>
<vertex x="2.1463" y="-0.2413"/>
<vertex x="2.9337" y="-0.2413"/>
<vertex x="2.9337" y="-0.635"/>
<vertex x="3.4163" y="-0.635"/>
<vertex x="3.4163" y="-0.2413"/>
<vertex x="3.81" y="-0.2413"/>
<vertex x="3.81" y="0.2413"/>
<vertex x="3.4163" y="0.2413"/>
<vertex x="3.4163" y="0.635"/>
<vertex x="2.9337" y="0.635"/>
<vertex x="2.9337" y="0.2413"/>
<vertex x="2.1463" y="0.2413"/>
<vertex x="2.1463" y="0.635"/>
<vertex x="1.6637" y="0.635"/>
<vertex x="1.6637" y="0.2413"/>
<vertex x="1.27" y="0.2413"/>
</polygon>
<wire x1="-2.3" y1="2.9" x2="-2.3" y2="-2.9" width="0.127" layer="51"/>
<wire x1="-2.3" y1="-2.9" x2="7.5" y2="-2.9" width="0.127" layer="51"/>
<wire x1="7.5" y1="-2.9" x2="7.5" y2="2.9" width="0.127" layer="51"/>
<wire x1="7.5" y1="2.9" x2="-2.3" y2="2.9" width="0.127" layer="51"/>
</package>
<package name="TC2030-MCP-NL-CP">
<description>&lt;B&gt;TAG-CONNECT ICSP Connector&lt;/B&gt;&lt;I&gt;- with optional copper pads for steel alignment pins&lt;/I&gt;&lt;BR&gt;&lt;I&gt;Manufacturer:&lt;/I&gt; &lt;a href="http://www.tag-connect.com"&gt;Tag-Connect&lt;/a&gt;&lt;BR&gt;
&lt;BR&gt;Cable for easy In-Circuit Serial Programming. Designed for Microchip ICD2, suitable for many others.
&lt;p&gt;
&lt;b&gt;NOTE:&lt;/b&gt; Eagle's default spacing for drill holes does not leave sufficent room for routing traces for this footprint and should be adjusted. &lt;br&gt;
This setting can be found in the board layout editor under the Edit menu.  Select "Design Rules" and then the Distance tab.  8 mils for Drill/Hole works well.
&lt;br&gt;
&lt;TABLE cellspacing=0 cellpadding=0 border=0&gt;
&lt;TR&gt;&lt;TD width=20&gt;&lt;/TD&gt;&lt;TD&gt;
&lt;TABLE cellspacing=0 cellpadding=1 border=1&gt;
&lt;TR bgcolor=silver&gt;&lt;TD align=center&gt;PAD&lt;/TD&gt;&lt;TD align=center&gt;Description&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;1&lt;/TD&gt;&lt;TD&gt;MCLR/Vpp&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;2&lt;/TD&gt;&lt;TD&gt;Vdd&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;3&lt;/TD&gt;&lt;TD&gt;GND&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;4&lt;/TD&gt;&lt;TD&gt;PGD (ISPDAT)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;5&lt;/TD&gt;&lt;TD&gt;PGC (ISPCLK)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;6&lt;/TD&gt;&lt;TD&gt;nc (used for LVP)&lt;/TD&gt;&lt;/TR&gt;
&lt;/TABLE&gt;
&lt;/TD&gt;&lt;/TR&gt;&lt;/TABLE&gt;&lt;BR&gt;&lt;BR&gt;

&lt;B&gt;Note:&lt;/B&gt; Suitable Receptacle pins are 0295-0-15-xx-06-xx-10-0 series from &lt;a href="www.mill-max.com"&gt;Mill-Max&lt;/a&gt;&lt;BR&gt;&lt;BR&gt;

©2009 ROFA.cz - modified and updated by Robert Darlington &amp;#8249;rdarlington@gmail.com&amp;#8250;</description>
<smd name="2" x="1.27" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="4" x="2.54" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="6" x="3.81" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="5" x="3.81" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="3" x="2.54" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="1" x="1.27" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<text x="-1.27" y="1.905" size="1.27" layer="25">&gt;name</text>
<hole x="0" y="0" drill="1.6"/>
<hole x="5.08" y="1.016" drill="1.6"/>
<hole x="5.08" y="-1.016" drill="1.6"/>
<polygon width="0.0254" layer="39">
<vertex x="1.27" y="-0.2413"/>
<vertex x="1.6637" y="-0.2413"/>
<vertex x="1.6637" y="-0.635"/>
<vertex x="2.1463" y="-0.635"/>
<vertex x="2.1463" y="-0.2413"/>
<vertex x="2.9337" y="-0.2413"/>
<vertex x="2.9337" y="-0.635"/>
<vertex x="3.4163" y="-0.635"/>
<vertex x="3.4163" y="-0.2413"/>
<vertex x="3.81" y="-0.2413"/>
<vertex x="3.81" y="0.2413"/>
<vertex x="3.4163" y="0.2413"/>
<vertex x="3.4163" y="0.635"/>
<vertex x="2.9337" y="0.635"/>
<vertex x="2.9337" y="0.2413"/>
<vertex x="2.1463" y="0.2413"/>
<vertex x="2.1463" y="0.635"/>
<vertex x="1.6637" y="0.635"/>
<vertex x="1.6637" y="0.2413"/>
<vertex x="1.27" y="0.2413"/>
</polygon>
<polygon width="0.0254" layer="16">
<vertex x="3.556" y="-1.016" curve="90"/>
<vertex x="5.08" y="-2.54" curve="90"/>
<vertex x="6.604" y="-1.016"/>
<vertex x="6.604" y="1.016" curve="90"/>
<vertex x="5.08" y="2.54" curve="90"/>
<vertex x="3.556" y="1.016"/>
</polygon>
<polygon width="0.0254" layer="16">
<vertex x="-1.524" y="0" curve="-90"/>
<vertex x="0" y="1.524" curve="-90"/>
<vertex x="1.524" y="0" curve="-90"/>
<vertex x="0" y="-1.524" curve="-90"/>
</polygon>
<polygon width="0.254" layer="30">
<vertex x="3.556" y="-1.016" curve="90"/>
<vertex x="5.08" y="-2.54" curve="90"/>
<vertex x="6.604" y="-1.016"/>
<vertex x="6.604" y="1.016" curve="90"/>
<vertex x="5.08" y="2.54" curve="90"/>
<vertex x="3.556" y="1.016"/>
</polygon>
<polygon width="0.254" layer="30">
<vertex x="-1.524" y="0" curve="-90"/>
<vertex x="0" y="1.524" curve="-90"/>
<vertex x="1.524" y="0" curve="-90"/>
<vertex x="0" y="-1.524" curve="-90"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="ATSAMD51">
<pin name="GND" x="-35.56" y="-132.08" length="middle"/>
<pin name="VDDCORE" x="-35.56" y="-45.72" length="middle"/>
<pin name="VDDANA" x="-35.56" y="-15.24" length="middle"/>
<pin name="VDDIO" x="-35.56" y="0" length="middle"/>
<pin name="PA00/XIN32/SER1-0/TC2-0" x="43.18" y="0" length="middle" rot="R180"/>
<pin name="PA01/XOUT32/SER1-1/TC2-1" x="43.18" y="-2.54" length="middle" rot="R180"/>
<pin name="PA02/ADC0-1/DAC-0" x="43.18" y="-5.08" length="middle" rot="R180"/>
<pin name="PA03/ANAREF-VREFA/ADC0-1" x="43.18" y="-7.62" length="middle" rot="R180"/>
<pin name="PA04/ANAREF-VREFB/ADC0-4/SER0-0/TC0-0" x="43.18" y="-10.16" length="middle" rot="R180"/>
<pin name="PA05/ADC0-5/DAC-1/SER0-1/TC0-1" x="43.18" y="-12.7" length="middle" rot="R180"/>
<pin name="PA06/ANAREF-VREFC/ADC0-6/SER0-2/TC1-0" x="43.18" y="-15.24" length="middle" rot="R180"/>
<pin name="PA07/ADC0-7/SER0-3/TC1-1" x="43.18" y="-17.78" length="middle" rot="R180"/>
<pin name="PA08/ADC0-8/ADC1-2/SER0-0/SER2-1/TC0-0/TCC0-0" x="43.18" y="-20.32" length="middle" rot="R180"/>
<pin name="PA09/ADC0-9/ADC1-3/SER0-1/SER2-0/TC0-1/TCC0-1" x="43.18" y="-22.86" length="middle" rot="R180"/>
<pin name="PA10/ADC0-10/SER0-2/SER2-2/TC1-0/TCC0-2" x="43.18" y="-25.4" length="middle" rot="R180"/>
<pin name="PA11/ADC0-11/SER0-3/SER2-3/TC1-1/TCC0-3" x="43.18" y="-27.94" length="middle" rot="R180"/>
<pin name="PA12/SER2-0/SER4-1/TC2-0/TCC0-6" x="43.18" y="-30.48" length="middle" rot="R180"/>
<pin name="PA13/SER2-1/SER4-0/TC2-1/TCC0-7" x="43.18" y="-33.02" length="middle" rot="R180"/>
<pin name="PA14/XIN0/SER2-2/SER4-2/TC3-0" x="43.18" y="-35.56" length="middle" rot="R180"/>
<pin name="PA15/XOUT0/SER2-3/SER4-3/TC3-1" x="43.18" y="-38.1" length="middle" rot="R180"/>
<pin name="PA16/SER1-0/SER3-1/TC2-0/TCC0-4" x="43.18" y="-40.64" length="middle" rot="R180"/>
<pin name="PA17/SER1-1/SER3-0/TC2-1/TCC0-5" x="43.18" y="-43.18" length="middle" rot="R180"/>
<pin name="PA18/SER1-2/SER3-2/TC3-0" x="43.18" y="-45.72" length="middle" rot="R180"/>
<pin name="PA19/SER1-3/SER3-3/TC3-1" x="43.18" y="-48.26" length="middle" rot="R180"/>
<pin name="PA20/SER5-2/SER3-2/TC7-0" x="43.18" y="-50.8" length="middle" rot="R180"/>
<pin name="PA21/SER5-3/SER3-3/TC7-1" x="43.18" y="-53.34" length="middle" rot="R180"/>
<pin name="PA22/SER3-0/SER5-1/TC4-0" x="43.18" y="-55.88" length="middle" rot="R180"/>
<pin name="PA23/SER3-1/SER5-0/TC4-1" x="43.18" y="-58.42" length="middle" rot="R180"/>
<pin name="PA24/SER3-2/SER5-2/TC5-0/PDEC0-0/USBDM" x="43.18" y="-60.96" length="middle" rot="R180"/>
<pin name="PA25/SER3-3/SER5-3/TC5-1/PDEC0-1/USBDP" x="43.18" y="-63.5" length="middle" rot="R180"/>
<pin name="PA27/GCLK-1" x="43.18" y="-66.04" length="middle" rot="R180"/>
<pin name="PA30/SER7-2/SER1-2/TC6-0/SWCLK" x="43.18" y="-68.58" length="middle" rot="R180"/>
<pin name="PA31/SER7-3/SER1-3/TC6-1/SWDIO" x="43.18" y="-71.12" length="middle" rot="R180"/>
<pin name="PB00/ADC0-12/SER5-2/TC7-0" x="43.18" y="-78.74" length="middle" rot="R180"/>
<pin name="PB01/ADC0-13/SER5-3/TC7-1" x="43.18" y="-81.28" length="middle" rot="R180"/>
<pin name="PB03/ADC0/SER5-1/TC6" x="43.18" y="-86.36" length="middle" rot="R180"/>
<pin name="PB04/ADC1-6" x="43.18" y="-88.9" length="middle" rot="R180"/>
<pin name="PB05/ADC1-7" x="43.18" y="-91.44" length="middle" rot="R180"/>
<pin name="PB06/ADC1-8" x="43.18" y="-93.98" length="middle" rot="R180"/>
<pin name="PB07/ADC1-9" x="43.18" y="-96.52" length="middle" rot="R180"/>
<pin name="PB08/ADC0-2/ADC1-9/SER4-0/TC4-0" x="43.18" y="-99.06" length="middle" rot="R180"/>
<pin name="PB09/ADC0-3/ADC1-1/SER4-1/TC4-1" x="43.18" y="-101.6" length="middle" rot="R180"/>
<pin name="PB10/SER4-2/TC5-0/TCC0-4" x="43.18" y="-104.14" length="middle" rot="R180"/>
<pin name="PB11/SER4-3/TC5-1/TCC0-5" x="43.18" y="-106.68" length="middle" rot="R180"/>
<pin name="PB12/SER4-0/TC4-0" x="43.18" y="-109.22" length="middle" rot="R180"/>
<pin name="PB13/SER4-1/TC4-1" x="43.18" y="-111.76" length="middle" rot="R180"/>
<pin name="PB14/SER4-2/TC5-0" x="43.18" y="-114.3" length="middle" rot="R180"/>
<pin name="PB15/SER4-3/TC5-1" x="43.18" y="-116.84" length="middle" rot="R180"/>
<pin name="PB16/SER5-0/TC6-0" x="43.18" y="-119.38" length="middle" rot="R180"/>
<pin name="PB17/SER5-1/TC6-1" x="43.18" y="-121.92" length="middle" rot="R180"/>
<pin name="PB22/XIN1/SER1-2/SER5-2/PDEC0-2/TC7-0" x="43.18" y="-124.46" length="middle" rot="R180"/>
<pin name="PB23/XOUT1/SER1-3/SER5-3/TC7-1" x="43.18" y="-127" length="middle" rot="R180"/>
<pin name="PB30/SER7-0/SER5-1/TC0-0/SWO" x="43.18" y="-129.54" length="middle" rot="R180"/>
<pin name="RESETN" x="-35.56" y="-119.38" length="middle"/>
<pin name="PB31/SER7-1/SER5-0/TC0-1" x="43.18" y="-132.08" length="middle" rot="R180"/>
<pin name="PB02/ADC0-14/SER5-0/TC6-0" x="43.18" y="-83.82" length="middle" rot="R180"/>
<wire x1="-30.48" y1="5.08" x2="38.1" y2="5.08" width="0.254" layer="94"/>
<wire x1="38.1" y1="5.08" x2="38.1" y2="-137.16" width="0.254" layer="94"/>
<wire x1="38.1" y1="-137.16" x2="-30.48" y2="-137.16" width="0.254" layer="94"/>
<wire x1="-30.48" y1="-137.16" x2="-30.48" y2="5.08" width="0.254" layer="94"/>
<text x="-5.08" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-142.24" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VSW" x="-35.56" y="-30.48" length="middle"/>
</symbol>
<symbol name="TC2030-SWD-ATSAM">
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="8.89" y2="-7.62" width="0.254" layer="94"/>
<wire x1="8.89" y1="-7.62" x2="8.89" y2="10.16" width="0.254" layer="94"/>
<wire x1="8.89" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<circle x="5.842" y="9.144" radius="0.5679" width="0.254" layer="94"/>
<circle x="4.826" y="3.048" radius="0.5679" width="0.254" layer="94"/>
<circle x="6.858" y="3.048" radius="0.5679" width="0.254" layer="94"/>
<pin name="VDD" x="-12.7" y="7.62" length="short" direction="pwr"/>
<pin name="RESET" x="-12.7" y="5.08" length="short" direction="pwr"/>
<pin name="GND" x="-12.7" y="2.54" length="short" direction="pwr"/>
<pin name="TRACESWO" x="-12.7" y="0" length="short" direction="pwr"/>
<pin name="SWCLK" x="-12.7" y="-2.54" length="short" direction="in"/>
<pin name="SWDIO" x="-12.7" y="-5.08" length="short"/>
<polygon width="0.254" layer="94">
<vertex x="6.096" y="7.62" curve="-90"/>
<vertex x="6.604" y="8.128" curve="-90"/>
<vertex x="7.112" y="7.62" curve="-90"/>
<vertex x="6.604" y="7.112" curve="-90"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="4.572" y="7.62" curve="-90"/>
<vertex x="5.08" y="8.128" curve="-90"/>
<vertex x="5.588" y="7.62" curve="-90"/>
<vertex x="5.08" y="7.112" curve="-90"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="6.096" y="6.096" curve="-90"/>
<vertex x="6.604" y="6.604" curve="-90"/>
<vertex x="7.112" y="6.096" curve="-90"/>
<vertex x="6.604" y="5.588" curve="-90"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="4.572" y="6.096" curve="-90"/>
<vertex x="5.08" y="6.604" curve="-90"/>
<vertex x="5.588" y="6.096" curve="-90"/>
<vertex x="5.08" y="5.588" curve="-90"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="4.572" y="4.572" curve="-90"/>
<vertex x="5.08" y="5.08" curve="-90"/>
<vertex x="5.588" y="4.572" curve="-90"/>
<vertex x="5.08" y="4.064" curve="-90"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="6.096" y="4.572" curve="-90"/>
<vertex x="6.604" y="5.08" curve="-90"/>
<vertex x="7.112" y="4.572" curve="-90"/>
<vertex x="6.604" y="4.064" curve="-90"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATSAMD51" prefix="U">
<gates>
<gate name="G$1" symbol="ATSAMD51" x="0" y="0"/>
</gates>
<devices>
<device name="QFN64" package="QFN-64-9X9MM-SMALLPAD">
<connects>
<connect gate="G$1" pin="GND" pad="7 22 33 47 54 P$1"/>
<connect gate="G$1" pin="PA00/XIN32/SER1-0/TC2-0" pad="1"/>
<connect gate="G$1" pin="PA01/XOUT32/SER1-1/TC2-1" pad="2"/>
<connect gate="G$1" pin="PA02/ADC0-1/DAC-0" pad="3"/>
<connect gate="G$1" pin="PA03/ANAREF-VREFA/ADC0-1" pad="4"/>
<connect gate="G$1" pin="PA04/ANAREF-VREFB/ADC0-4/SER0-0/TC0-0" pad="13"/>
<connect gate="G$1" pin="PA05/ADC0-5/DAC-1/SER0-1/TC0-1" pad="14"/>
<connect gate="G$1" pin="PA06/ANAREF-VREFC/ADC0-6/SER0-2/TC1-0" pad="15"/>
<connect gate="G$1" pin="PA07/ADC0-7/SER0-3/TC1-1" pad="16"/>
<connect gate="G$1" pin="PA08/ADC0-8/ADC1-2/SER0-0/SER2-1/TC0-0/TCC0-0" pad="17"/>
<connect gate="G$1" pin="PA09/ADC0-9/ADC1-3/SER0-1/SER2-0/TC0-1/TCC0-1" pad="18"/>
<connect gate="G$1" pin="PA10/ADC0-10/SER0-2/SER2-2/TC1-0/TCC0-2" pad="19"/>
<connect gate="G$1" pin="PA11/ADC0-11/SER0-3/SER2-3/TC1-1/TCC0-3" pad="20"/>
<connect gate="G$1" pin="PA12/SER2-0/SER4-1/TC2-0/TCC0-6" pad="29"/>
<connect gate="G$1" pin="PA13/SER2-1/SER4-0/TC2-1/TCC0-7" pad="30"/>
<connect gate="G$1" pin="PA14/XIN0/SER2-2/SER4-2/TC3-0" pad="31"/>
<connect gate="G$1" pin="PA15/XOUT0/SER2-3/SER4-3/TC3-1" pad="32"/>
<connect gate="G$1" pin="PA16/SER1-0/SER3-1/TC2-0/TCC0-4" pad="35"/>
<connect gate="G$1" pin="PA17/SER1-1/SER3-0/TC2-1/TCC0-5" pad="36"/>
<connect gate="G$1" pin="PA18/SER1-2/SER3-2/TC3-0" pad="37"/>
<connect gate="G$1" pin="PA19/SER1-3/SER3-3/TC3-1" pad="38"/>
<connect gate="G$1" pin="PA20/SER5-2/SER3-2/TC7-0" pad="41"/>
<connect gate="G$1" pin="PA21/SER5-3/SER3-3/TC7-1" pad="42"/>
<connect gate="G$1" pin="PA22/SER3-0/SER5-1/TC4-0" pad="43"/>
<connect gate="G$1" pin="PA23/SER3-1/SER5-0/TC4-1" pad="44"/>
<connect gate="G$1" pin="PA24/SER3-2/SER5-2/TC5-0/PDEC0-0/USBDM" pad="45"/>
<connect gate="G$1" pin="PA25/SER3-3/SER5-3/TC5-1/PDEC0-1/USBDP" pad="46"/>
<connect gate="G$1" pin="PA27/GCLK-1" pad="51"/>
<connect gate="G$1" pin="PA30/SER7-2/SER1-2/TC6-0/SWCLK" pad="57"/>
<connect gate="G$1" pin="PA31/SER7-3/SER1-3/TC6-1/SWDIO" pad="58"/>
<connect gate="G$1" pin="PB00/ADC0-12/SER5-2/TC7-0" pad="61"/>
<connect gate="G$1" pin="PB01/ADC0-13/SER5-3/TC7-1" pad="62"/>
<connect gate="G$1" pin="PB02/ADC0-14/SER5-0/TC6-0" pad="63"/>
<connect gate="G$1" pin="PB03/ADC0/SER5-1/TC6" pad="64"/>
<connect gate="G$1" pin="PB04/ADC1-6" pad="5"/>
<connect gate="G$1" pin="PB05/ADC1-7" pad="6"/>
<connect gate="G$1" pin="PB06/ADC1-8" pad="9"/>
<connect gate="G$1" pin="PB07/ADC1-9" pad="10"/>
<connect gate="G$1" pin="PB08/ADC0-2/ADC1-9/SER4-0/TC4-0" pad="11"/>
<connect gate="G$1" pin="PB09/ADC0-3/ADC1-1/SER4-1/TC4-1" pad="12"/>
<connect gate="G$1" pin="PB10/SER4-2/TC5-0/TCC0-4" pad="23"/>
<connect gate="G$1" pin="PB11/SER4-3/TC5-1/TCC0-5" pad="24"/>
<connect gate="G$1" pin="PB12/SER4-0/TC4-0" pad="25"/>
<connect gate="G$1" pin="PB13/SER4-1/TC4-1" pad="26"/>
<connect gate="G$1" pin="PB14/SER4-2/TC5-0" pad="27"/>
<connect gate="G$1" pin="PB15/SER4-3/TC5-1" pad="28"/>
<connect gate="G$1" pin="PB16/SER5-0/TC6-0" pad="39"/>
<connect gate="G$1" pin="PB17/SER5-1/TC6-1" pad="40"/>
<connect gate="G$1" pin="PB22/XIN1/SER1-2/SER5-2/PDEC0-2/TC7-0" pad="49"/>
<connect gate="G$1" pin="PB23/XOUT1/SER1-3/SER5-3/TC7-1" pad="50"/>
<connect gate="G$1" pin="PB30/SER7-0/SER5-1/TC0-0/SWO" pad="59"/>
<connect gate="G$1" pin="PB31/SER7-1/SER5-0/TC0-1" pad="60"/>
<connect gate="G$1" pin="RESETN" pad="52"/>
<connect gate="G$1" pin="VDDANA" pad="8"/>
<connect gate="G$1" pin="VDDCORE" pad="53"/>
<connect gate="G$1" pin="VDDIO" pad="21 34 48 56"/>
<connect gate="G$1" pin="VSW" pad="55"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TC2030-SWD-ATSAM" prefix="J" uservalue="yes">
<description>&lt;h3&gt;TAG-CONNECT ICSP Connector&lt;/h3&gt;&lt;BR&gt;&lt;I&gt;Manufacturer:&lt;/I&gt; &lt;a href="http://www.tag-connect.com"&gt;Tag-Connect&lt;/a&gt;&lt;BR&gt;
&lt;BR&gt;Cable for easy In-Circuit Serial Programming. Designed for Microchip ICD2, suitable for many others.&lt;BR&gt;
Two variants - one "with legs" (for hands-free fit on PCB) and another "without legs" for quick programming.

&lt;p&gt;
&lt;TABLE cellspacing=0 cellpadding=0 border=0&gt;
&lt;TR&gt;&lt;TD width=20&gt;&lt;/TD&gt;&lt;TD&gt;
&lt;TABLE cellspacing=0 cellpadding=1 border=1&gt;
&lt;TR bgcolor=silver&gt;&lt;TD align=center&gt;PAD&lt;/TD&gt;&lt;TD align=center&gt;Description&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;1&lt;/TD&gt;&lt;TD&gt;MCLR/Vpp&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;2&lt;/TD&gt;&lt;TD&gt;Vdd&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;3&lt;/TD&gt;&lt;TD&gt;GND&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;4&lt;/TD&gt;&lt;TD&gt;PGD (ISPDAT)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;5&lt;/TD&gt;&lt;TD&gt;PGC (ISPCLK)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;6&lt;/TD&gt;&lt;TD&gt;nc (used for LVP)&lt;/TD&gt;&lt;/TR&gt;
&lt;/TABLE&gt;
&lt;/TD&gt;&lt;/TR&gt;&lt;/TABLE&gt;&lt;BR&gt;&lt;BR&gt;

©2009 ROFA.cz - modified and updated by Robert Darlington &amp;#8249;rdarlington@gmail.com&amp;#8250;</description>
<gates>
<gate name="G$1" symbol="TC2030-SWD-ATSAM" x="0" y="0"/>
</gates>
<devices>
<device name="-MCP" package="TC2030-MCP">
<connects>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="RESET" pad="2"/>
<connect gate="G$1" pin="SWCLK" pad="4"/>
<connect gate="G$1" pin="SWDIO" pad="6"/>
<connect gate="G$1" pin="TRACESWO" pad="5"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-MCP-NL" package="TC2030-MCP-NL">
<connects>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="RESET" pad="2"/>
<connect gate="G$1" pin="SWCLK" pad="4"/>
<connect gate="G$1" pin="SWDIO" pad="6"/>
<connect gate="G$1" pin="TRACESWO" pad="5"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="TC2030-MCP-NL-CP">
<connects>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="RESET" pad="2"/>
<connect gate="G$1" pin="SWCLK" pad="4"/>
<connect gate="G$1" pin="SWDIO" pad="6"/>
<connect gate="G$1" pin="TRACESWO" pad="5"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="passives">
<packages>
<package name="TACT-SWITCH-KMR6">
<smd name="P$1" x="-2.05" y="0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$2" x="2.05" y="0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$3" x="-2.05" y="-0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$4" x="2.05" y="-0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<wire x1="-1.4" y1="0.8" x2="0" y2="0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="1.4" y2="0.8" width="0.127" layer="51"/>
<wire x1="-1.4" y1="-0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="1.4" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="0.6" width="0.127" layer="51"/>
<wire x1="0" y1="0.6" x2="0.4" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="0" y2="-0.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="0.2" x2="-2.1" y2="-0.2" width="0.127" layer="51"/>
<wire x1="2.1" y1="-0.2" x2="2.1" y2="0.2" width="0.127" layer="51"/>
<wire x1="2.1" y1="1.4" x2="2.1" y2="1.5" width="0.127" layer="51"/>
<wire x1="2.1" y1="1.5" x2="1" y2="1.5" width="0.127" layer="51"/>
<wire x1="1.032" y1="1.5" x2="-2.1" y2="1.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="1.5" x2="-2.1" y2="1.4" width="0.127" layer="51"/>
<wire x1="-2.1" y1="-1.4" x2="-2.1" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="-1.5" x2="2.1" y2="-1.5" width="0.127" layer="51"/>
<wire x1="2.1" y1="-1.5" x2="2.1" y2="-1.4" width="0.127" layer="51"/>
</package>
<package name="0603-CAP">
<wire x1="-0.356" y1="0.332" x2="0.356" y2="0.332" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.319" x2="0.356" y2="-0.319" width="0.1016" layer="51"/>
<smd name="1" x="-0.8" y="0" dx="0.96" dy="0.8" layer="1"/>
<smd name="2" x="0.8" y="0" dx="0.96" dy="0.8" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4" x2="-0.3381" y2="0.4" layer="51"/>
<rectangle x1="0.3302" y1="-0.4" x2="0.8303" y2="0.4" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0805">
<smd name="1" x="-0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.032" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="0402-CAP">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.4064" layer="21"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-2.413" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="1206">
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.794" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="2220-C">
<smd name="P$1" x="-2.6" y="0" dx="1.2" dy="5" layer="1"/>
<smd name="P$2" x="2.6" y="0" dx="1.2" dy="5" layer="1"/>
<text x="-1.5" y="3" size="0.6096" layer="125">&gt;NAME</text>
<text x="-1.5" y="-3.5" size="0.6096" layer="127">&gt;VALUE</text>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="51"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2.5" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2.5" layer="1"/>
<text x="-2.07" y="1.77" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.17" y="-3.24" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="744777920-INDUCTOR">
<smd name="P$1" x="0" y="3" dx="1.7" dy="2" layer="1"/>
<smd name="P$2" x="0" y="-3" dx="1.7" dy="2" layer="1"/>
<wire x1="-4" y1="0" x2="-4" y2="3" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="-3" y2="4" width="0.127" layer="21" curve="-90"/>
<wire x1="-3" y1="4" x2="3" y2="4" width="0.127" layer="21"/>
<wire x1="3" y1="4" x2="4" y2="3" width="0.127" layer="21" curve="-90"/>
<wire x1="4" y1="3" x2="4" y2="-3" width="0.127" layer="21"/>
<wire x1="4" y1="-3" x2="3" y2="-4" width="0.127" layer="21" curve="-90"/>
<wire x1="3" y1="-4" x2="-3" y2="-4" width="0.127" layer="21"/>
<wire x1="-3" y1="-4" x2="-4" y2="-3" width="0.127" layer="21" curve="-90"/>
<wire x1="-4" y1="-3" x2="-4" y2="0" width="0.127" layer="21"/>
<rectangle x1="-4" y1="-4" x2="4" y2="4" layer="39"/>
<text x="5.08" y="2.54" size="1.016" layer="25">&gt;NAME</text>
<text x="5.08" y="1.27" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="SPM6530-IND">
<smd name="1" x="0" y="2.775" dx="3.4" dy="1.85" layer="1"/>
<smd name="2" x="0" y="-2.775" dx="3.4" dy="1.85" layer="1"/>
<wire x1="-3.25" y1="3.85" x2="-3.25" y2="-3.85" width="0.127" layer="21"/>
<wire x1="-3.25" y1="-3.85" x2="3.25" y2="-3.85" width="0.127" layer="21"/>
<wire x1="3.25" y1="-3.85" x2="3.25" y2="3.85" width="0.127" layer="21"/>
<wire x1="3.25" y1="3.85" x2="-3.25" y2="3.85" width="0.127" layer="21"/>
<text x="3.81" y="2.54" size="1.016" layer="25">&gt;NAME</text>
<text x="3.81" y="-3.81" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="IHLP-5050FD-01-IND">
<smd name="1" x="0" y="5.4102" dx="4.953" dy="2.9464" layer="1"/>
<smd name="2" x="0" y="-5.4102" dx="4.953" dy="2.9464" layer="1"/>
<wire x1="6.4516" y1="6.604" x2="6.4516" y2="-6.604" width="0.127" layer="21"/>
<wire x1="3.81" y1="-6.604" x2="6.4516" y2="-6.604" width="0.127" layer="21"/>
<wire x1="6.4516" y1="6.604" x2="3.81" y2="6.604" width="0.127" layer="21"/>
<wire x1="-3.81" y1="6.604" x2="-6.4516" y2="6.604" width="0.127" layer="21"/>
<wire x1="-6.4516" y1="6.604" x2="-6.4516" y2="-6.604" width="0.127" layer="21"/>
<wire x1="-6.4516" y1="-6.604" x2="-3.81" y2="-6.604" width="0.127" layer="21"/>
<text x="5.08" y="7.62" size="1.016" layer="25">&gt;NAME</text>
<text x="5.08" y="-8.89" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="7443340330-IND">
<smd name="P$1" x="0" y="3.35" dx="3" dy="2.3" layer="1"/>
<smd name="P$2" x="0" y="-3.35" dx="3" dy="2.3" layer="1"/>
<wire x1="-2" y1="4" x2="-4" y2="4" width="0.127" layer="21"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.127" layer="21"/>
<wire x1="-4" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<wire x1="2" y1="-4" x2="4" y2="-4" width="0.127" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.127" layer="21"/>
<wire x1="4" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<text x="3" y="5" size="1.016" layer="25">&gt;NAME</text>
<text x="3" y="-6" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.15" y1="2.95" x2="1.15" y2="4.45" layer="51"/>
<rectangle x1="-1.15" y1="-4.45" x2="1.15" y2="-2.95" layer="51"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.778" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
<package name="8X8-IND">
<smd name="1" x="0" y="3.2" dx="2.2" dy="1.6" layer="1"/>
<smd name="2" x="0" y="-3.2" dx="2.2" dy="1.6" layer="1"/>
<wire x1="2" y1="-4" x2="4" y2="-4" width="0.127" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.127" layer="21"/>
<wire x1="4" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<wire x1="-2" y1="4" x2="-4" y2="4" width="0.127" layer="21"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.127" layer="21"/>
<wire x1="-4" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<text x="-5" y="5" size="1.27" layer="25">&gt;NAME</text>
<text x="-5" y="-6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-3.81" x2="3.81" y2="3.81" layer="39"/>
</package>
<package name="744029100-IND">
<smd name="1" x="0" y="1.1" dx="3.2" dy="1" layer="1"/>
<smd name="2" x="0" y="-1.1" dx="3.2" dy="1" layer="1"/>
<wire x1="-2" y1="2" x2="-2" y2="-2" width="0.127" layer="21"/>
<wire x1="-2" y1="-2" x2="2" y2="-2" width="0.127" layer="21"/>
<wire x1="2" y1="-2" x2="2" y2="2" width="0.127" layer="21"/>
<wire x1="2" y1="2" x2="-2" y2="2" width="0.127" layer="21"/>
<text x="-3" y="2.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="-3.6" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="7447709470-IND">
<smd name="1" x="0" y="4.95" dx="5.4" dy="2.9" layer="1"/>
<smd name="2" x="0" y="-4.95" dx="5.4" dy="2.9" layer="1"/>
<wire x1="-3" y1="6" x2="-6" y2="6" width="0.127" layer="21"/>
<wire x1="-6" y1="6" x2="-6" y2="-6" width="0.127" layer="21"/>
<wire x1="-6" y1="-6" x2="-3" y2="-6" width="0.127" layer="21"/>
<wire x1="3" y1="-6" x2="6" y2="-6" width="0.127" layer="21"/>
<wire x1="6" y1="-6" x2="6" y2="6" width="0.127" layer="21"/>
<wire x1="6" y1="6" x2="3" y2="6" width="0.127" layer="21"/>
<text x="-7" y="8" size="1.27" layer="25">&gt;NAME</text>
<text x="-7" y="-9" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="7447789002-IND">
<smd name="1" x="0" y="3" dx="1.7" dy="2" layer="1"/>
<smd name="2" x="0" y="-3" dx="1.7" dy="2" layer="1"/>
<wire x1="2" y1="-4" x2="4" y2="-4" width="0.127" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.127" layer="21"/>
<wire x1="4" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<wire x1="-2" y1="4" x2="-4" y2="4" width="0.127" layer="21"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.127" layer="21"/>
<wire x1="-4" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<text x="-5" y="5" size="1.27" layer="25">&gt;NAME</text>
<text x="-5" y="-6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-3.81" x2="3.81" y2="3.81" layer="39"/>
</package>
<package name="2-SMD-3.2X1.5MM">
<smd name="P$1" x="-1.25" y="0" dx="1.9" dy="1.1" layer="1" rot="R90"/>
<smd name="P$2" x="1.25" y="0" dx="1.9" dy="1.1" layer="1" rot="R90"/>
<wire x1="-0.6" y1="0.9" x2="0.6" y2="0.9" width="0.127" layer="51"/>
<wire x1="-0.6" y1="-0.9" x2="0.6" y2="-0.9" width="0.127" layer="51"/>
<text x="-2.54" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TACT-SWITCH-SIDE">
<smd name="P$1" x="-1.8" y="0.725" dx="1.4" dy="1.05" layer="1" rot="R180"/>
<smd name="P$2" x="1.8" y="0.725" dx="1.4" dy="1.05" layer="1" rot="R180"/>
<smd name="P$3" x="-1.8" y="-0.725" dx="1.4" dy="1.05" layer="1" rot="R180"/>
<smd name="P$4" x="1.8" y="-0.725" dx="1.4" dy="1.05" layer="1" rot="R180"/>
<wire x1="-0.9" y1="0.8" x2="0" y2="0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0.9" y2="0.8" width="0.127" layer="51"/>
<wire x1="-0.9" y1="-0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="0.9" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="0.6" width="0.127" layer="51"/>
<wire x1="0" y1="0.6" x2="0.4" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="0" y2="-0.5" width="0.127" layer="51"/>
<wire x1="-1.75" y1="-1.45" x2="1.75" y2="-1.45" width="0.127" layer="21"/>
<wire x1="-1.75" y1="1.6" x2="-1" y2="1.6" width="0.127" layer="21"/>
<wire x1="-1" y1="1.6" x2="0" y2="1.6" width="0.127" layer="21"/>
<wire x1="0" y1="1.6" x2="1" y2="1.6" width="0.127" layer="21"/>
<wire x1="1" y1="1.6" x2="1.75" y2="1.6" width="0.127" layer="21"/>
<wire x1="-1" y1="1.6" x2="-1" y2="2.3" width="0.127" layer="21"/>
<wire x1="-1" y1="2.3" x2="1" y2="2.3" width="0.127" layer="21"/>
<wire x1="1" y1="2.3" x2="1" y2="1.6" width="0.127" layer="21"/>
</package>
<package name="NRS5020">
<smd name="P$1" x="-1.8" y="0" dx="1.5" dy="4" layer="1"/>
<smd name="P$2" x="1.8" y="0" dx="1.5" dy="4" layer="1"/>
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="51"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.127" layer="51"/>
<wire x1="2.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="2.5" width="0.127" layer="51"/>
</package>
<package name="4-SMD-3.2X2.5">
<smd name="P$1" x="-1.1" y="-0.8" dx="1.4" dy="1.2" layer="1"/>
<smd name="P$2" x="1.1" y="-0.8" dx="1.4" dy="1.2" layer="1"/>
<smd name="P$3" x="1.1" y="0.8" dx="1.4" dy="1.2" layer="1"/>
<smd name="P$4" x="-1.1" y="0.8" dx="1.4" dy="1.2" layer="1"/>
<rectangle x1="-1.6" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
<circle x="-2" y="1.6" radius="0.14141875" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="TS2">
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-3.175" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-4.445" y2="0" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0" x2="-4.445" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<circle x="0" y="-2.54" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="2.54" radius="0.127" width="0.4064" layer="94"/>
<text x="-6.35" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="3.175" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="S1" x="2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="P1" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
</symbol>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.524" y="-4.064" size="1.27" layer="97">&gt;PACKAGE</text>
<text x="1.524" y="-5.842" size="1.27" layer="97">&gt;VOLTAGE</text>
<text x="1.524" y="-7.62" size="1.27" layer="97">&gt;TYPE</text>
</symbol>
<symbol name="INDUCTOR">
<wire x1="0" y1="5.08" x2="1.27" y2="3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<text x="-1.27" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="3.81" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="2" x="0" y="-7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="1" x="0" y="7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<text x="6.35" y="-5.08" size="1.27" layer="97" rot="R90">&gt;PACKAGE</text>
</symbol>
<symbol name="RESONATOR">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="2.54" y="1.016" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="CRYSTAL-MHZ">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="2.54" y="1.016" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="GND" x="0" y="-7.62" length="middle" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="2-8X4-5_SWITCH" prefix="S">
<gates>
<gate name="G$1" symbol="TS2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TACT-SWITCH-KMR6">
<connects>
<connect gate="G$1" pin="P" pad="P$1"/>
<connect gate="G$1" pin="P1" pad="P$2"/>
<connect gate="G$1" pin="S" pad="P$3"/>
<connect gate="G$1" pin="S1" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDE" package="TACT-SWITCH-SIDE">
<connects>
<connect gate="G$1" pin="P" pad="P$1"/>
<connect gate="G$1" pin="P1" pad="P$2"/>
<connect gate="G$1" pin="S" pad="P$3"/>
<connect gate="G$1" pin="S1" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0402-CAP" package="0402-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0402"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1210" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2220" package="2220-C">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="INDUCTOR" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-744777920" package="744777920-INDUCTOR">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
</technology>
</technologies>
</device>
<device name="-0603" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
</technology>
</technologies>
</device>
<device name="-SPM6530" package="SPM6530-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-IHLP-5050FD-01" package="IHLP-5050FD-01-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7443340330" package="7443340330-IND">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="7443340330"/>
</technology>
</technologies>
</device>
<device name="-0402" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0402"/>
</technology>
</technologies>
</device>
<device name="-744778002" package="8X8-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-744029100" package="744029100-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7447709470" package="7447709470-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7447789002" package="7447789002-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="NRS5020">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="KHZ-CRYSTAL" prefix="Y">
<gates>
<gate name="G$1" symbol="RESONATOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2-SMD-3.2X1.5MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MHZ-CRYSTAL" prefix="Y">
<gates>
<gate name="G$1" symbol="CRYSTAL-MHZ" x="0" y="0"/>
</gates>
<devices>
<device name="" package="4-SMD-3.2X2.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$3"/>
<connect gate="G$1" pin="GND" pad="P$2 P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="comm">
<packages>
<package name="DFN-12-4X3">
<wire x1="-1.5" y1="2" x2="1.5" y2="2" width="0.127" layer="51"/>
<wire x1="1.5" y1="-2" x2="1.5" y2="2" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-2" x2="1.5" y2="-2" width="0.127" layer="51"/>
<smd name="7" x="1.45" y="-1.25" dx="0.25" dy="0.7" layer="1" rot="R90"/>
<smd name="PAD" x="0" y="0" dx="0.2" dy="0.2" layer="1" rot="R180"/>
<polygon width="0.127" layer="1">
<vertex x="0.7" y="-1.25"/>
<vertex x="-0.75" y="-1.25"/>
<vertex x="-0.75" y="0.8"/>
<vertex x="-0.3" y="1.25"/>
<vertex x="0.7" y="1.25"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="0.6" y="-1.15"/>
<vertex x="-0.65" y="-1.15"/>
<vertex x="-0.65" y="0.75"/>
<vertex x="-0.25" y="1.15"/>
<vertex x="0.6" y="1.15"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-0.75" y="0.8"/>
<vertex x="-0.75" y="-1.25"/>
<vertex x="0.7" y="-1.25"/>
<vertex x="0.7" y="1.25"/>
<vertex x="-0.3" y="1.25"/>
</polygon>
<text x="0" y="-2.151" size="0.6096" layer="25" font="vector" ratio="20" rot="R180" align="bottom-center">&gt;NAME</text>
<text x="0" y="2.151" size="0.6096" layer="27" font="vector" ratio="20" rot="R180" align="top-center">&gt;VALUE</text>
<wire x1="-1.5" y1="-2" x2="-1.5" y2="2" width="0.127" layer="51"/>
<smd name="8" x="1.45" y="-0.75" dx="0.25" dy="0.7" layer="1" rot="R90"/>
<smd name="9" x="1.45" y="-0.25" dx="0.25" dy="0.7" layer="1" rot="R90"/>
<smd name="10" x="1.45" y="0.25" dx="0.25" dy="0.7" layer="1" rot="R90"/>
<smd name="11" x="1.45" y="0.75" dx="0.25" dy="0.7" layer="1" rot="R90"/>
<smd name="12" x="1.45" y="1.25" dx="0.25" dy="0.7" layer="1" rot="R90"/>
<smd name="1" x="-1.45" y="1.25" dx="0.25" dy="0.7" layer="1" rot="R270"/>
<smd name="2" x="-1.45" y="0.75" dx="0.25" dy="0.7" layer="1" rot="R270"/>
<smd name="3" x="-1.45" y="0.25" dx="0.25" dy="0.7" layer="1" rot="R270"/>
<smd name="4" x="-1.45" y="-0.25" dx="0.25" dy="0.7" layer="1" rot="R270"/>
<smd name="5" x="-1.45" y="-0.75" dx="0.25" dy="0.7" layer="1" rot="R270"/>
<smd name="6" x="-1.45" y="-1.25" dx="0.25" dy="0.7" layer="1" rot="R270"/>
<wire x1="-1" y1="2" x2="-1.5" y2="1.5" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="RS485-1-1-LTC2855">
<pin name="B" x="17.78" y="-12.7" length="middle" rot="R180"/>
<pin name="A" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="RO" x="-17.78" y="-10.16" length="middle"/>
<pin name="DE" x="-17.78" y="-17.78" length="middle"/>
<pin name="GND" x="-17.78" y="7.62" length="middle"/>
<pin name="Y" x="17.78" y="-22.86" length="middle" rot="R180"/>
<pin name="Z" x="17.78" y="-17.78" length="middle" rot="R180"/>
<pin name="DI" x="-17.78" y="-22.86" length="middle"/>
<pin name="VCC" x="-17.78" y="15.24" length="middle"/>
<text x="-2.54" y="17.78" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-27.94" size="1.27" layer="96">&gt;VALUE</text>
<wire x1="-12.7" y1="17.78" x2="-12.7" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-25.4" x2="12.7" y2="-25.4" width="0.254" layer="94"/>
<wire x1="12.7" y1="-25.4" x2="12.7" y2="17.78" width="0.254" layer="94"/>
<wire x1="12.7" y1="17.78" x2="-12.7" y2="17.78" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-10.16" x2="0" y2="-10.16" width="0.254" layer="94"/>
<wire x1="0" y1="-10.16" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="-7.62" x2="2.54" y2="-12.7" width="0.254" layer="94"/>
<wire x1="2.54" y1="-12.7" x2="0" y2="-10.16" width="0.254" layer="94"/>
<wire x1="0" y1="-17.78" x2="2.54" y2="-20.32" width="0.254" layer="94"/>
<wire x1="2.54" y1="-20.32" x2="0" y2="-22.86" width="0.254" layer="94"/>
<wire x1="0" y1="-22.86" x2="0" y2="-20.32" width="0.254" layer="94"/>
<wire x1="0" y1="-20.32" x2="0" y2="-17.78" width="0.254" layer="94"/>
<wire x1="2.54" y1="-17.78" x2="2.54" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-22.86" x2="-2.54" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-22.86" x2="-2.54" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-20.32" x2="0" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-17.78" x2="0" y2="-17.78" width="0.254" layer="94"/>
<circle x="3.302" y="-12.192" radius="0.254" width="0.254" layer="94"/>
<circle x="3.302" y="-18.034" radius="0.254" width="0.254" layer="94"/>
<pin name="!RE!" x="-17.78" y="-5.08" length="middle"/>
<pin name="TE" x="-17.78" y="0" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RS485-1-1-LTC2855">
<gates>
<gate name="G$1" symbol="RS485-1-1-LTC2855" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DFN-12-4X3">
<connects>
<connect gate="G$1" pin="!RE!" pad="2"/>
<connect gate="G$1" pin="A" pad="11"/>
<connect gate="G$1" pin="B" pad="10"/>
<connect gate="G$1" pin="DE" pad="3"/>
<connect gate="G$1" pin="DI" pad="4"/>
<connect gate="G$1" pin="GND" pad="6 PAD"/>
<connect gate="G$1" pin="RO" pad="1"/>
<connect gate="G$1" pin="TE" pad="5"/>
<connect gate="G$1" pin="VCC" pad="12"/>
<connect gate="G$1" pin="Y" pad="8"/>
<connect gate="G$1" pin="Z" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="connector">
<packages>
<package name="FIDUCIAL_1MM">
<smd name="1" x="0" y="0" dx="1" dy="1" layer="1" roundness="100" stop="no" cream="no"/>
<polygon width="0.127" layer="29">
<vertex x="-1" y="0" curve="90"/>
<vertex x="0" y="-1" curve="90"/>
<vertex x="1" y="0" curve="90"/>
<vertex x="0" y="1" curve="90"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="-1" y="0" curve="90"/>
<vertex x="0" y="-1" curve="90"/>
<vertex x="1" y="0" curve="90"/>
<vertex x="0" y="1" curve="90"/>
</polygon>
<circle x="0" y="0" radius="0.4953" width="0" layer="51"/>
</package>
<package name="FIDUCIAL_RECT_1MM">
<smd name="P$1" x="0" y="0" dx="1" dy="1" layer="1"/>
<rectangle x1="-1" y1="-1" x2="1" y2="1" layer="39"/>
<rectangle x1="-1" y1="-1" x2="1" y2="1" layer="29"/>
<rectangle x1="-1" y1="-1" x2="1" y2="1" layer="41"/>
</package>
<package name="DX4R005HJ5_100">
<wire x1="3.25" y1="-2.6" x2="-3.25" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-3.25" y1="2.6" x2="-3.25" y2="0" width="0.127" layer="51"/>
<wire x1="3.25" y1="2.6" x2="3.25" y2="0" width="0.127" layer="51"/>
<wire x1="-1.75" y1="2.6" x2="1.75" y2="2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-2.2" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="3.25" y1="-2.6" x2="3.25" y2="-2.2" width="0.127" layer="51"/>
<smd name="GND@3" x="-2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@4" x="2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@1" x="-2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="GND@2" x="2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="D+" x="0" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="D-" x="-0.65" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="VBUS" x="-1.3" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="ID" x="0.65" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="GND" x="1.3" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<text x="4.1275" y="-1.5875" size="0.6096" layer="27" font="vector" rot="R90">&gt;Value</text>
<text x="-3.4925" y="-1.27" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
</package>
<package name="DX4R005HJ5">
<wire x1="3.25" y1="-2.6" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="2.6" x2="-3.25" y2="0" width="0.127" layer="21"/>
<wire x1="3.25" y1="2.6" x2="3.25" y2="0" width="0.127" layer="21"/>
<wire x1="-1.75" y1="2.6" x2="1.75" y2="2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-2.2" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="3.25" y1="-2.6" x2="3.25" y2="-2.2" width="0.127" layer="51"/>
<smd name="GND@3" x="-2.475" y="-1.1" dx="2.75" dy="1.9" layer="1"/>
<smd name="GND@4" x="2.475" y="-1.1" dx="2.75" dy="1.9" layer="1"/>
<smd name="GND@1" x="-2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="GND@2" x="2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="D+" x="0" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<smd name="D-" x="-0.65" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<smd name="VBUS" x="-1.3" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<smd name="ID" x="0.65" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<smd name="GND" x="1.3" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<text x="-3.4925" y="-1.27" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
<text x="4.1275" y="-1.5875" size="0.6096" layer="25" font="vector" rot="R90">&gt;Value</text>
</package>
<package name="DX4R005HJ5_64">
<wire x1="3.25" y1="-2.6" x2="-3.25" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-3.25" y1="2.6" x2="-3.25" y2="0" width="0.127" layer="51"/>
<wire x1="3.25" y1="2.6" x2="3.25" y2="0" width="0.127" layer="51"/>
<wire x1="-1.75" y1="2.6" x2="1.75" y2="2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-2.2" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="3.25" y1="-2.6" x2="3.25" y2="-2.2" width="0.127" layer="51"/>
<smd name="GND@3" x="-2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@4" x="2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@1" x="-2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="GND@2" x="2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="D+" x="0" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="D-" x="-0.65" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="VBUS" x="-1.3" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="ID" x="0.65" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="GND" x="1.3" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<text x="-3.4925" y="-1.27" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
<text x="4.1275" y="-1.5875" size="0.6096" layer="27" font="vector" rot="R90">&gt;Value</text>
</package>
<package name="SJFAB">
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="0" x2="-1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-0.7874" y="0" dx="1.1176" dy="1.6002" layer="1"/>
<smd name="2" x="0.7874" y="0" dx="1.1176" dy="1.6002" layer="1"/>
<text x="-1.651" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.4001" y="0" size="0.02" layer="27">&gt;VALUE</text>
</package>
<package name="TOMBSTONE">
<wire x1="-1" y1="-1" x2="-1" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-1" y1="-0.6" x2="-1" y2="-0.2" width="0.127" layer="21"/>
<wire x1="-1" y1="-0.2" x2="-1" y2="0.2" width="0.127" layer="21"/>
<wire x1="-1" y1="0.2" x2="-1" y2="0.5" width="0.127" layer="21"/>
<wire x1="-1" y1="0.5" x2="1" y2="0.5" width="0.127" layer="21" curve="-180"/>
<wire x1="1" y1="0.5" x2="1" y2="0.2" width="0.127" layer="21"/>
<wire x1="1" y1="0.2" x2="1" y2="-0.2" width="0.127" layer="21"/>
<wire x1="1" y1="-0.2" x2="1" y2="-0.6" width="0.127" layer="21"/>
<wire x1="1" y1="-0.6" x2="1" y2="-1" width="0.127" layer="21"/>
<wire x1="1" y1="-1" x2="0.6" y2="-1" width="0.127" layer="21"/>
<wire x1="0.6" y1="-1" x2="0.2" y2="-1" width="0.127" layer="21"/>
<wire x1="0.2" y1="-1" x2="-0.2" y2="-1" width="0.127" layer="21"/>
<wire x1="-0.2" y1="-1" x2="-0.6" y2="-1" width="0.127" layer="21"/>
<wire x1="-0.6" y1="-1" x2="-1" y2="-1" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-1" y="-1"/>
<vertex x="1" y="-1"/>
<vertex x="1" y="0.5" curve="90"/>
<vertex x="0" y="1.5" curve="90"/>
<vertex x="-1" y="0.5"/>
</polygon>
</package>
<package name="CBA-SILK-LOGO">
<circle x="0" y="0" radius="0.254" width="0.127" layer="21"/>
<circle x="-0.762" y="0.762" radius="0.254" width="0.127" layer="21"/>
<wire x1="-0.254" y1="1.016" x2="0.254" y2="1.016" width="0.127" layer="21"/>
<wire x1="0.254" y1="1.016" x2="0.254" y2="0.508" width="0.127" layer="21"/>
<wire x1="0.254" y1="0.508" x2="-0.254" y2="0.508" width="0.127" layer="21"/>
<wire x1="-0.254" y1="0.508" x2="-0.254" y2="1.016" width="0.127" layer="21"/>
<wire x1="-1.016" y1="0.254" x2="-0.508" y2="0.254" width="0.127" layer="21"/>
<wire x1="-0.508" y1="0.254" x2="-0.508" y2="-0.254" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-0.254" x2="-1.016" y2="-0.254" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-0.254" x2="-1.016" y2="0.254" width="0.127" layer="21"/>
<wire x1="0.508" y1="0.508" x2="1.016" y2="0.508" width="0.127" layer="21"/>
<wire x1="1.016" y1="0.508" x2="1.016" y2="1.016" width="0.127" layer="21"/>
<wire x1="1.016" y1="1.016" x2="0.508" y2="1.016" width="0.127" layer="21"/>
<wire x1="0.508" y1="1.016" x2="0.508" y2="0.508" width="0.127" layer="21"/>
<wire x1="0.508" y1="0.254" x2="1.016" y2="0.254" width="0.127" layer="21"/>
<wire x1="1.016" y1="0.254" x2="1.016" y2="-0.254" width="0.127" layer="21"/>
<wire x1="1.016" y1="-0.254" x2="0.508" y2="-0.254" width="0.127" layer="21"/>
<wire x1="0.508" y1="-0.254" x2="0.508" y2="0.254" width="0.127" layer="21"/>
<wire x1="0.508" y1="-0.508" x2="1.016" y2="-0.508" width="0.127" layer="21"/>
<wire x1="1.016" y1="-0.508" x2="1.016" y2="-1.016" width="0.127" layer="21"/>
<wire x1="1.016" y1="-1.016" x2="0.508" y2="-1.016" width="0.127" layer="21"/>
<wire x1="0.508" y1="-1.016" x2="0.508" y2="-0.508" width="0.127" layer="21"/>
<wire x1="0.254" y1="-0.508" x2="-0.254" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-0.508" x2="-0.254" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-1.016" x2="0.254" y2="-1.016" width="0.127" layer="21"/>
<wire x1="0.254" y1="-1.016" x2="0.254" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-0.508" x2="-1.016" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-0.508" x2="-1.016" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-1.016" x2="-0.508" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-1.016" x2="-0.508" y2="-0.508" width="0.127" layer="21"/>
</package>
<package name="RJ10-4-SMT-SQUIDPORT">
<smd name="P$1" x="-1.905" y="8.75" dx="3.4" dy="0.76" layer="1" rot="R90"/>
<smd name="P$2" x="-0.635" y="8.75" dx="3.4" dy="0.76" layer="1" rot="R90"/>
<smd name="P$3" x="0.635" y="8.75" dx="3.4" dy="0.76" layer="1" rot="R90"/>
<smd name="P$4" x="1.905" y="8.75" dx="3.4" dy="0.76" layer="1" rot="R90"/>
<smd name="P$7" x="-5.2" y="0" dx="5.5" dy="2.35" layer="1" rot="R90"/>
<smd name="P$8" x="5.2" y="0" dx="5.5" dy="2.35" layer="1" rot="R90"/>
<wire x1="-5.25" y1="-0.75" x2="-5.25" y2="0.25" width="0.127" layer="21"/>
<wire x1="5.25" y1="-0.75" x2="5.25" y2="0.25" width="0.127" layer="21"/>
<wire x1="-5.55" y1="-5.5" x2="-5.55" y2="7" width="0.0762" layer="51"/>
<wire x1="-5.55" y1="7" x2="5.55" y2="7" width="0.0762" layer="51"/>
<wire x1="5.55" y1="7" x2="5.55" y2="-5.5" width="0.0762" layer="51"/>
<wire x1="5.55" y1="-5.5" x2="-5.55" y2="-5.5" width="0.0762" layer="51"/>
<wire x1="-5.4" y1="6.8" x2="-5" y2="6.8" width="0.1524" layer="21"/>
<wire x1="-5" y1="6.8" x2="-5.4" y2="6.4" width="0.1524" layer="21"/>
<wire x1="-5.4" y1="6.4" x2="-5.4" y2="6.8" width="0.1524" layer="21"/>
<wire x1="5.4" y1="6.8" x2="5.4" y2="6.4" width="0.1524" layer="21"/>
<wire x1="5.4" y1="6.4" x2="5" y2="6.8" width="0.1524" layer="21"/>
<wire x1="5" y1="6.8" x2="5.4" y2="6.8" width="0.1524" layer="21"/>
<wire x1="-5.4" y1="-5.3" x2="-5.4" y2="-4.9" width="0.1524" layer="21"/>
<wire x1="-5.4" y1="-4.9" x2="-5" y2="-5.3" width="0.1524" layer="21"/>
<wire x1="-5" y1="-5.3" x2="-5.4" y2="-5.3" width="0.1524" layer="21"/>
<wire x1="5.4" y1="-5.3" x2="5" y2="-5.3" width="0.1524" layer="21"/>
<wire x1="5" y1="-5.3" x2="5.4" y2="-4.9" width="0.1524" layer="21"/>
<wire x1="5.4" y1="-4.9" x2="5.4" y2="-5.3" width="0.1524" layer="21"/>
<wire x1="4.6" y1="9.3" x2="4.6" y2="8.7" width="0.0762" layer="47"/>
<wire x1="4.3" y1="9" x2="4.9" y2="9" width="0.0762" layer="47"/>
<wire x1="-4.3" y1="10" x2="-4.3" y2="9.6" width="0.0762" layer="47"/>
<wire x1="-4.5" y1="9.8" x2="-4.1" y2="9.8" width="0.0762" layer="47"/>
<wire x1="-4.3" y1="8.1" x2="-4.3" y2="7.7" width="0.0762" layer="47"/>
<wire x1="6.3" y1="12.7" x2="6.3" y2="12.3" width="0.0762" layer="47"/>
<wire x1="6.1" y1="12.5" x2="6.5" y2="12.5" width="0.0762" layer="47"/>
<wire x1="-4.2" y1="-6.2" x2="-4" y2="-6.2" width="0.0762" layer="47"/>
<wire x1="4" y1="-6.2" x2="4.2" y2="-6.2" width="0.0762" layer="47"/>
<wire x1="-4.1" y1="-6.1" x2="-4.1" y2="-6.3" width="0.0762" layer="47"/>
<wire x1="4.1" y1="-6.1" x2="4.1" y2="-6.3" width="0.0762" layer="47"/>
<text x="-4.8" y="-4.7" size="0.6096" layer="21" font="vector" align="center-left">YLW</text>
<text x="4.8" y="-4.7" size="0.6096" layer="21" font="vector" rot="R180" align="center-left">GRN</text>
<text x="-2.5" y="-6.5" size="0.6096" layer="21" font="vector">RX</text>
<text x="2.5" y="-6.5" size="0.6096" layer="21" font="vector" align="bottom-right">TX</text>
<wire x1="-5.5" y1="-7" x2="5.5" y2="-7" width="0.0762" layer="47"/>
<wire x1="-0.1" y1="-6.7" x2="0.1" y2="-6.7" width="0.0762" layer="47"/>
<wire x1="0" y1="-6.6" x2="0" y2="-6.8" width="0.0762" layer="47"/>
<wire x1="0" y1="11.4" x2="0" y2="11.2" width="0.0762" layer="47"/>
<wire x1="-0.1" y1="11.3" x2="0.1" y2="11.3" width="0.0762" layer="47"/>
<wire x1="-4.5" y1="7.9" x2="-4.1" y2="7.9" width="0.0762" layer="47"/>
<text x="-4.8" y="9.8" size="0.6096" layer="47" font="vector" rot="R180" align="center-left">YLW</text>
<text x="-4.8" y="7.9" size="0.6096" layer="47" font="vector" rot="R180" align="center-left">GRN</text>
<wire x1="0" y1="-0.3" x2="0" y2="0.3" width="0.0762" layer="21"/>
<wire x1="-0.3" y1="0" x2="0.3" y2="0" width="0.0762" layer="21"/>
</package>
<package name="609-4613-1-ND">
<smd name="HD0" x="-3.8" y="0" dx="1.9" dy="1.8" layer="1"/>
<smd name="HD4" x="-3.1" y="2.55" dx="2.1" dy="1.6" layer="1"/>
<smd name="HD5" x="3.1" y="2.55" dx="2.1" dy="1.6" layer="1"/>
<smd name="D+" x="0" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<smd name="D-" x="-0.65" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<smd name="VBUS" x="-1.3" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<smd name="ID" x="0.65" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<smd name="GND" x="1.3" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<text x="4.9275" y="1.2125" size="0.6096" layer="27" font="vector" rot="R90">&gt;Value</text>
<text x="-4.3925" y="1.13" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
<smd name="HD1" x="-1.05" y="0" dx="1.9" dy="1.8" layer="1"/>
<smd name="HD2" x="1.05" y="0" dx="1.9" dy="1.8" layer="1"/>
<smd name="HD3" x="3.8" y="0" dx="1.9" dy="1.8" layer="1"/>
<wire x1="-4.7" y1="-1.45" x2="4.7" y2="-1.45" width="0.127" layer="51"/>
<text x="0" y="-1.3" size="0.8128" layer="51" font="vector" align="bottom-center">\\ PCB Edge /</text>
<wire x1="-3.9" y1="3" x2="-3.9" y2="-2.5" width="0.127" layer="51"/>
<wire x1="-3.9" y1="-2.5" x2="3.9" y2="-2.5" width="0.127" layer="51"/>
<wire x1="3.9" y1="-2.5" x2="3.9" y2="3" width="0.127" layer="51"/>
<wire x1="3.9" y1="3" x2="-3.9" y2="3" width="0.127" layer="51"/>
<wire x1="-3.9" y1="1.1" x2="-3.9" y2="1.5" width="0.127" layer="21"/>
<wire x1="3.9" y1="1.1" x2="3.9" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.8" y1="3" x2="1.7" y2="3" width="0.127" layer="21"/>
<wire x1="-1.7" y1="3" x2="-1.8" y2="3" width="0.127" layer="21"/>
<wire x1="4.4" y1="3" x2="4.7" y2="3" width="0.127" layer="21"/>
<wire x1="-4.4" y1="3" x2="-4.7" y2="3" width="0.127" layer="21"/>
<wire x1="-3.9" y1="3.6" x2="-3.9" y2="3.8" width="0.127" layer="21"/>
<wire x1="3.9" y1="3.6" x2="3.9" y2="3.8" width="0.127" layer="21"/>
</package>
<package name="LITE-TRAP-MOLEX-203863">
<smd name="P$1" x="-3.05" y="0" dx="1" dy="1" layer="1" stop="no" thermals="no" cream="no"/>
<wire x1="-1.65" y1="1.5" x2="4.15" y2="1.5" width="0.0762" layer="51"/>
<wire x1="-1.65" y1="-1.5" x2="4.15" y2="-1.5" width="0.0762" layer="51"/>
<circle x="0" y="0" radius="1" width="0.0762" layer="51"/>
<wire x1="-3.5" y1="0" x2="-4" y2="0.5" width="0.0762" layer="51"/>
<wire x1="-4" y1="0.5" x2="-4" y2="-0.5" width="0.0762" layer="51"/>
<wire x1="-4" y1="-0.5" x2="-3.5" y2="0" width="0.0762" layer="51"/>
<polygon width="0.1" layer="1">
<vertex x="-1.9" y="1.25"/>
<vertex x="4.2" y="1.25"/>
<vertex x="4.2" y="-1.25"/>
<vertex x="-1.9" y="-1.25"/>
<vertex x="-1.9" y="-1.7"/>
<vertex x="-4.2" y="-1.7"/>
<vertex x="-4.2" y="1.7"/>
<vertex x="-1.9" y="1.7"/>
</polygon>
<polygon width="0.1" layer="31">
<vertex x="-4.2" y="1.7"/>
<vertex x="-1.9" y="1.7"/>
<vertex x="-1.9" y="1.25"/>
<vertex x="4.2" y="1.25"/>
<vertex x="4.2" y="-1.25"/>
<vertex x="-1.9" y="-1.25"/>
<vertex x="-1.9" y="-1.65"/>
<vertex x="-1.9" y="-1.7"/>
<vertex x="-4.2" y="-1.7"/>
</polygon>
<polygon width="0.1" layer="29">
<vertex x="-4.35" y="1.85"/>
<vertex x="-1.75" y="1.85"/>
<vertex x="-1.75" y="1.4"/>
<vertex x="4.35" y="1.4"/>
<vertex x="4.35" y="-1.4"/>
<vertex x="-1.75" y="-1.4"/>
<vertex x="-1.75" y="-1.85"/>
<vertex x="-4.35" y="-1.85"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="DOT">
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="USB-1">
<wire x1="6.35" y1="-2.54" x2="6.35" y2="2.54" width="0.254" layer="94"/>
<wire x1="6.35" y1="2.54" x2="-3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="-2.54" width="0.254" layer="94"/>
<text x="-2.54" y="-1.27" size="2.54" layer="94">USB</text>
<text x="-4.445" y="-1.905" size="1.27" layer="95" font="vector" rot="R90">&gt;Name</text>
<text x="8.255" y="-1.905" size="1.27" layer="96" font="vector" rot="R90">&gt;Value</text>
<pin name="D+" x="5.08" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="D-" x="2.54" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="VBUS" x="0" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="GND" x="-2.54" y="5.08" visible="pad" length="short" rot="R270"/>
</symbol>
<symbol name="SJFAB">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="TOMBSTONE">
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94" curve="-180"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="CBALOGO">
<text x="0" y="0" size="1.27" layer="97" align="center">~ center for bits and atoms ~
cba.mit.edu</text>
</symbol>
<symbol name="RJ10-CONN">
<description>&lt;h3&gt;6 Pin Connection&lt;/h3&gt;</description>
<wire x1="3.81" y1="-10.16" x2="-10.16" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-7.62" x2="0" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-10.16" x2="3.81" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-10.16" y1="10.16" x2="3.81" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.6096" layer="94"/>
<text x="-10.16" y="-12.446" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-10.16" y="10.668" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="RX-A" x="5.08" y="7.62" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="RX-B" x="5.08" y="2.54" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="TX-Z" x="5.08" y="-2.54" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="TX-Y" x="5.08" y="-7.62" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="CONN_01">
<description>&lt;h3&gt;2 Pin Connection&lt;/h3&gt;</description>
<wire x1="3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<text x="-2.54" y="-4.826" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-2.54" y="3.048" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FIDUCIAL" prefix="J">
<description>For use by pick and place machines to calibrate the vision/machine, 1mm
&lt;p&gt;By microbuilder.eu&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="DOT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FIDUCIAL_1MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RCT" package="FIDUCIAL_RECT_1MM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MICRO-USB" prefix="X">
<description>SMD micro USB connector as found in the fablab inventory. 
Three footprint variants included: 
&lt;ol&gt;
&lt;li&gt; original, as described by manufacturer's datasheet
&lt;li&gt; for milling with the 1/100" bit
&lt;li&gt; for milling with the 1/64" bit
&lt;/ol&gt;
&lt;p&gt;Made by Zaerc.</description>
<gates>
<gate name="G$1" symbol="USB-1" x="0" y="0"/>
</gates>
<devices>
<device name="_1/100" package="DX4R005HJ5_100">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_ORIG" package="DX4R005HJ5">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1/64" package="DX4R005HJ5_64">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="609-4613-1-ND">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SOLDER_JUMPER" prefix="J">
<gates>
<gate name="G$1" symbol="SJFAB" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SJFAB">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TOMBSTONE" prefix="MP">
<gates>
<gate name="G$1" symbol="TOMBSTONE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TOMBSTONE">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CBA-LOGO">
<gates>
<gate name="G$1" symbol="CBALOGO" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CBA-SILK-LOGO">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RJ10">
<gates>
<gate name="G$1" symbol="RJ10-CONN" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="RJ10-4-SMT-SQUIDPORT">
<connects>
<connect gate="G$1" pin="RX-A" pad="P$4"/>
<connect gate="G$1" pin="RX-B" pad="P$3"/>
<connect gate="G$1" pin="TX-Y" pad="P$1"/>
<connect gate="G$1" pin="TX-Z" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="POKEPOWER">
<gates>
<gate name="G$1" symbol="CONN_01" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LITE-TRAP-MOLEX-203863">
<connects>
<connect gate="G$1" pin="2" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="D3" library="lights" deviceset="LED" device="0805"/>
<part name="R3" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="D2" library="lights" deviceset="LED" device="0805"/>
<part name="R2" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="D1" library="lights" deviceset="LED" device="0805"/>
<part name="R1" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="+3V33" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="S1" library="passives" deviceset="2-8X4-5_SWITCH" device=""/>
<part name="R4" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="R5" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="U3" library="power" deviceset="VREG-AP2112" device=""/>
<part name="+3V34" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C13" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="J4" library="connector" deviceset="FIDUCIAL" device="RCT" value="FIDUCIALRCT"/>
<part name="J5" library="connector" deviceset="FIDUCIAL" device="RCT" value="FIDUCIALRCT"/>
<part name="X1" library="connector" deviceset="MICRO-USB" device="" value="MICRO-USB"/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="J1" library="connector" deviceset="SOLDER_JUMPER" device=""/>
<part name="U5" library="microcontrollers" deviceset="ATSAMD51" device="QFN64"/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="C3" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="U$1" library="comm" deviceset="RS485-1-1-LTC2855" device=""/>
<part name="U$2" library="comm" deviceset="RS485-1-1-LTC2855" device=""/>
<part name="R6" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="10k"/>
<part name="+3V2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="C1" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C2" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="L1" library="passives" deviceset="INDUCTOR" device="-0805" value="10uH"/>
<part name="C4" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="R7" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="10k"/>
<part name="C5" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C6" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C7" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C8" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="D4" library="lights" deviceset="LED" device="0805"/>
<part name="R8" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J6" library="microcontrollers" deviceset="TC2030-SWD-ATSAM" device="-MCP-NL"/>
<part name="+3V7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C10" library="passives" deviceset="CAP" device="0603-CAP" value="12.5pH"/>
<part name="C11" library="passives" deviceset="CAP" device="0603-CAP" value="12.5pF"/>
<part name="Y2" library="passives" deviceset="KHZ-CRYSTAL" device=""/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="MP1" library="connector" deviceset="TOMBSTONE" device=""/>
<part name="U$3" library="connector" deviceset="CBA-LOGO" device=""/>
<part name="U$4" library="connector" deviceset="RJ10" device=""/>
<part name="U$5" library="connector" deviceset="RJ10" device=""/>
<part name="C12" library="passives" deviceset="CAP" device="0603-CAP" value="12.5pH"/>
<part name="C14" library="passives" deviceset="CAP" device="0603-CAP" value="12.5pF"/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C15" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C16" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="D5" library="lights" deviceset="LED" device="0805"/>
<part name="D6" library="lights" deviceset="LED" device="0805"/>
<part name="C17" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="R9" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="R10" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="D7" library="lights" deviceset="LED" device="0805"/>
<part name="D8" library="lights" deviceset="LED" device="0805"/>
<part name="Y1" library="passives" deviceset="MHZ-CRYSTAL" device=""/>
<part name="R11" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="R12" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="U$6" library="comm" deviceset="RS485-1-1-LTC2855" device=""/>
<part name="+3V1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U$7" library="connector" deviceset="RJ10" device=""/>
<part name="C18" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="D9" library="lights" deviceset="LED" device="0805"/>
<part name="D10" library="lights" deviceset="LED" device="0805"/>
<part name="R13" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="R14" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="U$8" library="comm" deviceset="RS485-1-1-LTC2855" device=""/>
<part name="+3V4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND20" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U$9" library="connector" deviceset="RJ10" device=""/>
<part name="C19" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="D11" library="lights" deviceset="LED" device="0805"/>
<part name="D12" library="lights" deviceset="LED" device="0805"/>
<part name="R15" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="R16" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="U$10" library="comm" deviceset="RS485-1-1-LTC2855" device=""/>
<part name="+3V8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND23" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U$11" library="connector" deviceset="RJ10" device=""/>
<part name="C20" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="D13" library="lights" deviceset="LED" device="0805"/>
<part name="D14" library="lights" deviceset="LED" device="0805"/>
<part name="U$12" library="comm" deviceset="RS485-1-1-LTC2855" device=""/>
<part name="+3V9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND26" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U$13" library="connector" deviceset="RJ10" device=""/>
<part name="C21" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="R17" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="R18" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="D15" library="lights" deviceset="LED" device="0805"/>
<part name="D16" library="lights" deviceset="LED" device="0805"/>
<part name="+3V10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V18" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V19" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V20" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V21" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="U$14" library="connector" deviceset="POKEPOWER" device=""/>
<part name="U$15" library="connector" deviceset="POKEPOWER" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="218.44" y="228.6" size="1.778" layer="97" align="top-left">PORTA: SER3
PORTB: SER4
PORTC: SER0 
PORTD: SER1
PORTE: SER5
PORTF: SER2 </text>
</plain>
<instances>
<instance part="D3" gate="G$1" x="190.5" y="375.92" smashed="yes" rot="R270">
<attribute name="NAME" x="188.468" y="372.364" size="1.778" layer="95"/>
<attribute name="VALUE" x="188.468" y="370.205" size="1.778" layer="96"/>
</instance>
<instance part="R3" gate="G$1" x="177.8" y="375.92" smashed="yes">
<attribute name="NAME" x="173.99" y="377.4186" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.99" y="372.618" size="1.778" layer="96"/>
<attribute name="PRECISION" x="173.99" y="369.062" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="173.99" y="370.84" size="1.27" layer="97"/>
</instance>
<instance part="D2" gate="G$1" x="190.5" y="386.08" smashed="yes" rot="R270">
<attribute name="NAME" x="188.468" y="382.524" size="1.778" layer="95"/>
<attribute name="VALUE" x="188.468" y="380.365" size="1.778" layer="96"/>
</instance>
<instance part="R2" gate="G$1" x="177.8" y="386.08" smashed="yes">
<attribute name="NAME" x="173.99" y="387.5786" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.99" y="382.778" size="1.778" layer="96"/>
<attribute name="PRECISION" x="173.99" y="379.222" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="173.99" y="381" size="1.27" layer="97"/>
</instance>
<instance part="D1" gate="G$1" x="190.5" y="396.24" smashed="yes" rot="R270">
<attribute name="NAME" x="188.468" y="392.684" size="1.778" layer="95"/>
<attribute name="VALUE" x="188.468" y="390.525" size="1.778" layer="96"/>
</instance>
<instance part="R1" gate="G$1" x="177.8" y="396.24" smashed="yes">
<attribute name="NAME" x="173.99" y="397.7386" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.99" y="392.938" size="1.778" layer="96"/>
<attribute name="PRECISION" x="173.99" y="389.382" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="173.99" y="391.16" size="1.27" layer="97"/>
</instance>
<instance part="+3V33" gate="G$1" x="223.52" y="381" smashed="yes" rot="R270">
<attribute name="VALUE" x="218.44" y="383.54" size="1.778" layer="96"/>
</instance>
<instance part="GND5" gate="1" x="160.02" y="396.24" smashed="yes" rot="R270">
<attribute name="VALUE" x="157.48" y="398.78" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="S1" gate="G$1" x="66.04" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="68.58" y="156.21" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="62.865" y="158.75" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R4" gate="G$1" x="302.26" y="261.62" smashed="yes">
<attribute name="NAME" x="298.45" y="263.1186" size="1.778" layer="95"/>
<attribute name="VALUE" x="298.45" y="258.318" size="1.778" layer="96"/>
<attribute name="PRECISION" x="298.45" y="254.762" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="298.45" y="256.54" size="1.27" layer="97"/>
</instance>
<instance part="R5" gate="G$1" x="302.26" y="274.32" smashed="yes">
<attribute name="NAME" x="298.45" y="275.8186" size="1.778" layer="95"/>
<attribute name="VALUE" x="298.45" y="271.018" size="1.778" layer="96"/>
<attribute name="PRECISION" x="298.45" y="267.462" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="298.45" y="269.24" size="1.27" layer="97"/>
</instance>
<instance part="U3" gate="G$1" x="177.8" y="335.28" smashed="yes">
<attribute name="NAME" x="175.26" y="342.9" size="1.27" layer="95"/>
<attribute name="VALUE" x="180.34" y="327.66" size="1.27" layer="96"/>
</instance>
<instance part="+3V34" gate="G$1" x="223.52" y="337.82" smashed="yes" rot="R270">
<attribute name="VALUE" x="218.44" y="340.36" size="1.778" layer="96"/>
</instance>
<instance part="GND6" gate="1" x="177.8" y="317.5" smashed="yes">
<attribute name="VALUE" x="175.26" y="314.96" size="1.778" layer="96"/>
</instance>
<instance part="C13" gate="G$1" x="193.04" y="332.74" smashed="yes">
<attribute name="NAME" x="194.564" y="335.661" size="1.778" layer="95"/>
<attribute name="VALUE" x="194.564" y="330.581" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="194.564" y="328.676" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="194.564" y="326.898" size="1.27" layer="97"/>
<attribute name="TYPE" x="194.564" y="325.12" size="1.27" layer="97"/>
</instance>
<instance part="J4" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="J5" gate="G$1" x="381" y="381" smashed="yes"/>
<instance part="X1" gate="G$1" x="144.78" y="355.6" smashed="yes" rot="R270">
<attribute name="NAME" x="142.875" y="360.045" size="1.27" layer="95" font="vector"/>
<attribute name="VALUE" x="142.875" y="347.345" size="1.27" layer="96" font="vector"/>
</instance>
<instance part="GND12" gate="1" x="165.1" y="360.68" smashed="yes" rot="R180">
<attribute name="VALUE" x="167.64" y="363.22" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+5" gate="1" x="203.2" y="355.6" smashed="yes" rot="R270">
<attribute name="VALUE" x="198.12" y="358.14" size="1.778" layer="96"/>
</instance>
<instance part="J1" gate="G$1" x="182.88" y="355.6" smashed="yes" rot="R180">
<attribute name="NAME" x="185.42" y="353.06" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="185.42" y="359.41" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="U5" gate="G$1" x="139.7" y="297.18" smashed="yes">
<attribute name="NAME" x="134.62" y="304.8" size="1.778" layer="95"/>
<attribute name="VALUE" x="134.62" y="154.94" size="1.778" layer="96"/>
</instance>
<instance part="P+2" gate="1" x="132.08" y="342.9" smashed="yes">
<attribute name="VALUE" x="129.54" y="337.82" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C3" gate="G$1" x="205.74" y="332.74" smashed="yes">
<attribute name="NAME" x="207.264" y="335.661" size="1.778" layer="95"/>
<attribute name="VALUE" x="207.264" y="330.581" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="207.264" y="328.676" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="207.264" y="326.898" size="1.27" layer="97"/>
<attribute name="TYPE" x="207.264" y="325.12" size="1.27" layer="97"/>
</instance>
<instance part="U$1" gate="G$1" x="365.76" y="81.28" smashed="yes">
<attribute name="NAME" x="363.22" y="99.06" size="1.27" layer="95"/>
<attribute name="VALUE" x="363.22" y="53.34" size="1.27" layer="96"/>
</instance>
<instance part="U$2" gate="G$1" x="365.76" y="284.48" smashed="yes">
<attribute name="NAME" x="363.22" y="302.26" size="1.27" layer="95"/>
<attribute name="VALUE" x="363.22" y="256.54" size="1.27" layer="96"/>
</instance>
<instance part="R6" gate="G$1" x="78.74" y="190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="77.2414" y="186.69" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="82.042" y="186.69" size="1.778" layer="96" rot="R90"/>
<attribute name="PRECISION" x="85.598" y="186.69" size="1.27" layer="97" rot="R90"/>
<attribute name="PACKAGE" x="83.82" y="186.69" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="+3V2" gate="G$1" x="78.74" y="215.9" smashed="yes">
<attribute name="VALUE" x="76.2" y="210.82" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND3" gate="1" x="96.52" y="154.94" smashed="yes">
<attribute name="VALUE" x="93.98" y="152.4" size="1.778" layer="96"/>
</instance>
<instance part="+3V3" gate="G$1" x="30.48" y="297.18" smashed="yes" rot="R90">
<attribute name="VALUE" x="35.56" y="294.64" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C1" gate="G$1" x="91.44" y="246.38" smashed="yes">
<attribute name="NAME" x="92.964" y="249.301" size="1.778" layer="95"/>
<attribute name="VALUE" x="92.964" y="244.221" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="92.964" y="242.316" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="92.964" y="240.538" size="1.27" layer="97"/>
<attribute name="TYPE" x="92.964" y="238.76" size="1.27" layer="97"/>
</instance>
<instance part="C2" gate="G$1" x="78.74" y="246.38" smashed="yes">
<attribute name="NAME" x="80.264" y="249.301" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.264" y="244.221" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="80.264" y="242.316" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="80.264" y="240.538" size="1.27" layer="97"/>
<attribute name="TYPE" x="80.264" y="238.76" size="1.27" layer="97"/>
</instance>
<instance part="GND9" gate="1" x="78.74" y="233.68" smashed="yes">
<attribute name="VALUE" x="76.2" y="231.14" size="1.778" layer="96"/>
</instance>
<instance part="L1" gate="G$1" x="88.9" y="266.7" smashed="yes" rot="R270">
<attribute name="NAME" x="83.82" y="267.97" size="1.778" layer="95"/>
<attribute name="VALUE" x="83.82" y="262.89" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="83.82" y="260.35" size="1.27" layer="97"/>
</instance>
<instance part="C4" gate="G$1" x="78.74" y="170.18" smashed="yes">
<attribute name="NAME" x="80.264" y="173.101" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.264" y="168.021" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="80.264" y="166.116" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="80.264" y="164.338" size="1.27" layer="97"/>
<attribute name="TYPE" x="80.264" y="162.56" size="1.27" layer="97"/>
</instance>
<instance part="R7" gate="G$1" x="71.12" y="200.66" smashed="yes" rot="R90">
<attribute name="NAME" x="69.6214" y="196.85" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="74.422" y="196.85" size="1.778" layer="96" rot="R90"/>
<attribute name="PRECISION" x="77.978" y="196.85" size="1.27" layer="97" rot="R90"/>
<attribute name="PACKAGE" x="76.2" y="196.85" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="C5" gate="G$1" x="78.74" y="292.1" smashed="yes">
<attribute name="NAME" x="80.264" y="295.021" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.264" y="289.941" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="80.264" y="288.036" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="80.264" y="286.258" size="1.27" layer="97"/>
<attribute name="TYPE" x="80.264" y="284.48" size="1.27" layer="97"/>
</instance>
<instance part="C6" gate="G$1" x="66.04" y="292.1" smashed="yes">
<attribute name="NAME" x="67.564" y="295.021" size="1.778" layer="95"/>
<attribute name="VALUE" x="67.564" y="289.941" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="67.564" y="288.036" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="67.564" y="286.258" size="1.27" layer="97"/>
<attribute name="TYPE" x="67.564" y="284.48" size="1.27" layer="97"/>
</instance>
<instance part="C7" gate="G$1" x="53.34" y="292.1" smashed="yes">
<attribute name="NAME" x="54.864" y="295.021" size="1.778" layer="95"/>
<attribute name="VALUE" x="54.864" y="289.941" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="54.864" y="288.036" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="54.864" y="286.258" size="1.27" layer="97"/>
<attribute name="TYPE" x="54.864" y="284.48" size="1.27" layer="97"/>
</instance>
<instance part="C8" gate="G$1" x="40.64" y="292.1" smashed="yes">
<attribute name="NAME" x="42.164" y="295.021" size="1.778" layer="95"/>
<attribute name="VALUE" x="42.164" y="289.941" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="42.164" y="288.036" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="42.164" y="286.258" size="1.27" layer="97"/>
<attribute name="TYPE" x="42.164" y="284.48" size="1.27" layer="97"/>
</instance>
<instance part="GND2" gate="1" x="40.64" y="279.4" smashed="yes">
<attribute name="VALUE" x="38.1" y="276.86" size="1.778" layer="96"/>
</instance>
<instance part="+3V5" gate="G$1" x="320.04" y="96.52" smashed="yes" rot="R90">
<attribute name="VALUE" x="325.12" y="93.98" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="+3V6" gate="G$1" x="320.04" y="299.72" smashed="yes" rot="R90">
<attribute name="VALUE" x="325.12" y="297.18" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND8" gate="1" x="320.04" y="88.9" smashed="yes" rot="R270">
<attribute name="VALUE" x="317.5" y="91.44" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND10" gate="1" x="320.04" y="292.1" smashed="yes" rot="R270">
<attribute name="VALUE" x="317.5" y="294.64" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="D4" gate="G$1" x="220.98" y="238.76" smashed="yes" rot="R90">
<attribute name="NAME" x="223.012" y="242.316" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="223.012" y="244.475" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R8" gate="G$1" x="236.22" y="238.76" smashed="yes">
<attribute name="NAME" x="232.41" y="240.2586" size="1.778" layer="95"/>
<attribute name="VALUE" x="232.41" y="235.458" size="1.778" layer="96"/>
<attribute name="PRECISION" x="232.41" y="231.902" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="232.41" y="233.68" size="1.27" layer="97"/>
</instance>
<instance part="GND11" gate="1" x="251.46" y="238.76" smashed="yes" rot="R90">
<attribute name="VALUE" x="254" y="236.22" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="J6" gate="G$1" x="38.1" y="190.5" smashed="yes"/>
<instance part="+3V7" gate="G$1" x="7.62" y="205.74" smashed="yes">
<attribute name="VALUE" x="5.08" y="200.66" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND13" gate="1" x="7.62" y="180.34" smashed="yes">
<attribute name="VALUE" x="5.08" y="177.8" size="1.778" layer="96"/>
</instance>
<instance part="C10" gate="G$1" x="233.68" y="299.72" smashed="yes" rot="R90">
<attribute name="NAME" x="230.759" y="301.244" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="235.839" y="301.244" size="1.778" layer="96" rot="R90"/>
<attribute name="PACKAGE" x="237.744" y="301.244" size="1.27" layer="97" rot="R90"/>
<attribute name="VOLTAGE" x="239.522" y="301.244" size="1.27" layer="97" rot="R90"/>
<attribute name="TYPE" x="241.3" y="301.244" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="C11" gate="G$1" x="231.14" y="294.64" smashed="yes" rot="R270">
<attribute name="NAME" x="234.061" y="293.116" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="228.981" y="293.116" size="1.778" layer="96" rot="R270"/>
<attribute name="PACKAGE" x="227.076" y="293.116" size="1.27" layer="97" rot="R270"/>
<attribute name="VOLTAGE" x="225.298" y="293.116" size="1.27" layer="97" rot="R270"/>
<attribute name="TYPE" x="223.52" y="293.116" size="1.27" layer="97" rot="R270"/>
</instance>
<instance part="Y2" gate="G$1" x="220.98" y="297.18" smashed="yes" rot="R90">
<attribute name="NAME" x="219.964" y="299.72" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="223.52" y="299.72" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND1" gate="1" x="241.3" y="297.18" smashed="yes" rot="R90">
<attribute name="VALUE" x="243.84" y="294.64" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="MP1" gate="G$1" x="101.6" y="124.46" smashed="yes"/>
<instance part="U$3" gate="G$1" x="78.74" y="124.46" smashed="yes"/>
<instance part="U$4" gate="G$1" x="393.7" y="292.1" smashed="yes">
<attribute name="VALUE" x="388.62" y="279.654" size="1.778" layer="96" font="vector"/>
<attribute name="NAME" x="388.62" y="302.768" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="U$5" gate="G$1" x="393.7" y="88.9" smashed="yes">
<attribute name="VALUE" x="388.62" y="76.454" size="1.778" layer="96" font="vector"/>
<attribute name="NAME" x="388.62" y="99.568" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="C12" gate="G$1" x="233.68" y="264.16" smashed="yes" rot="R90">
<attribute name="NAME" x="230.759" y="265.684" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="235.839" y="265.684" size="1.778" layer="96" rot="R90"/>
<attribute name="PACKAGE" x="237.744" y="265.684" size="1.27" layer="97" rot="R90"/>
<attribute name="VOLTAGE" x="239.522" y="265.684" size="1.27" layer="97" rot="R90"/>
<attribute name="TYPE" x="241.3" y="265.684" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="C14" gate="G$1" x="231.14" y="259.08" smashed="yes" rot="R270">
<attribute name="NAME" x="234.061" y="257.556" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="228.981" y="257.556" size="1.778" layer="96" rot="R270"/>
<attribute name="PACKAGE" x="227.076" y="257.556" size="1.27" layer="97" rot="R270"/>
<attribute name="VOLTAGE" x="225.298" y="257.556" size="1.27" layer="97" rot="R270"/>
<attribute name="TYPE" x="223.52" y="257.556" size="1.27" layer="97" rot="R270"/>
</instance>
<instance part="GND4" gate="1" x="241.3" y="261.62" smashed="yes" rot="R90">
<attribute name="VALUE" x="243.84" y="259.08" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C15" gate="G$1" x="327.66" y="91.44" smashed="yes">
<attribute name="NAME" x="329.184" y="94.361" size="1.778" layer="95"/>
<attribute name="VALUE" x="329.184" y="89.281" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="329.184" y="87.376" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="329.184" y="85.598" size="1.27" layer="97"/>
<attribute name="TYPE" x="329.184" y="83.82" size="1.27" layer="97"/>
</instance>
<instance part="C16" gate="G$1" x="327.66" y="294.64" smashed="yes">
<attribute name="NAME" x="329.184" y="297.561" size="1.778" layer="95"/>
<attribute name="VALUE" x="329.184" y="292.481" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="329.184" y="290.576" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="329.184" y="288.798" size="1.27" layer="97"/>
<attribute name="TYPE" x="329.184" y="287.02" size="1.27" layer="97"/>
</instance>
<instance part="D5" gate="G$1" x="312.42" y="261.62" smashed="yes" rot="R270">
<attribute name="NAME" x="310.388" y="258.064" size="1.778" layer="95"/>
<attribute name="VALUE" x="310.388" y="266.065" size="1.778" layer="96"/>
</instance>
<instance part="D6" gate="G$1" x="312.42" y="274.32" smashed="yes" rot="R270">
<attribute name="NAME" x="310.388" y="270.764" size="1.778" layer="95"/>
<attribute name="VALUE" x="307.848" y="278.765" size="1.778" layer="96"/>
</instance>
<instance part="C17" gate="G$1" x="91.44" y="292.1" smashed="yes">
<attribute name="NAME" x="92.964" y="295.021" size="1.778" layer="95"/>
<attribute name="VALUE" x="92.964" y="289.941" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="92.964" y="288.036" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="92.964" y="286.258" size="1.27" layer="97"/>
<attribute name="TYPE" x="92.964" y="284.48" size="1.27" layer="97"/>
</instance>
<instance part="R9" gate="G$1" x="302.26" y="58.42" smashed="yes">
<attribute name="NAME" x="298.45" y="59.9186" size="1.778" layer="95"/>
<attribute name="VALUE" x="298.45" y="55.118" size="1.778" layer="96"/>
<attribute name="PRECISION" x="298.45" y="51.562" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="298.45" y="53.34" size="1.27" layer="97"/>
</instance>
<instance part="R10" gate="G$1" x="302.26" y="71.12" smashed="yes">
<attribute name="NAME" x="298.45" y="72.6186" size="1.778" layer="95"/>
<attribute name="VALUE" x="298.45" y="67.818" size="1.778" layer="96"/>
<attribute name="PRECISION" x="298.45" y="64.262" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="298.45" y="66.04" size="1.27" layer="97"/>
</instance>
<instance part="D7" gate="G$1" x="312.42" y="58.42" smashed="yes" rot="R270">
<attribute name="NAME" x="310.388" y="54.864" size="1.778" layer="95"/>
<attribute name="VALUE" x="310.388" y="62.865" size="1.778" layer="96"/>
</instance>
<instance part="D8" gate="G$1" x="312.42" y="71.12" smashed="yes" rot="R270">
<attribute name="NAME" x="310.388" y="67.564" size="1.778" layer="95"/>
<attribute name="VALUE" x="307.848" y="75.565" size="1.778" layer="96"/>
</instance>
<instance part="Y1" gate="G$1" x="220.98" y="261.62" smashed="yes" rot="R90">
<attribute name="NAME" x="219.964" y="264.16" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="223.52" y="264.16" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R11" gate="G$1" x="302.26" y="210.82" smashed="yes">
<attribute name="NAME" x="298.45" y="212.3186" size="1.778" layer="95"/>
<attribute name="VALUE" x="298.45" y="207.518" size="1.778" layer="96"/>
<attribute name="PRECISION" x="298.45" y="203.962" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="298.45" y="205.74" size="1.27" layer="97"/>
</instance>
<instance part="R12" gate="G$1" x="302.26" y="223.52" smashed="yes">
<attribute name="NAME" x="298.45" y="225.0186" size="1.778" layer="95"/>
<attribute name="VALUE" x="298.45" y="220.218" size="1.778" layer="96"/>
<attribute name="PRECISION" x="298.45" y="216.662" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="298.45" y="218.44" size="1.27" layer="97"/>
</instance>
<instance part="U$6" gate="G$1" x="365.76" y="233.68" smashed="yes">
<attribute name="NAME" x="363.22" y="251.46" size="1.27" layer="95"/>
<attribute name="VALUE" x="363.22" y="205.74" size="1.27" layer="96"/>
</instance>
<instance part="+3V1" gate="G$1" x="320.04" y="248.92" smashed="yes" rot="R90">
<attribute name="VALUE" x="325.12" y="246.38" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND7" gate="1" x="320.04" y="241.3" smashed="yes" rot="R270">
<attribute name="VALUE" x="317.5" y="243.84" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="U$7" gate="G$1" x="393.7" y="241.3" smashed="yes">
<attribute name="VALUE" x="388.62" y="228.854" size="1.778" layer="96" font="vector"/>
<attribute name="NAME" x="388.62" y="251.968" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="C18" gate="G$1" x="327.66" y="243.84" smashed="yes">
<attribute name="NAME" x="329.184" y="246.761" size="1.778" layer="95"/>
<attribute name="VALUE" x="329.184" y="241.681" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="329.184" y="239.776" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="329.184" y="237.998" size="1.27" layer="97"/>
<attribute name="TYPE" x="329.184" y="236.22" size="1.27" layer="97"/>
</instance>
<instance part="D9" gate="G$1" x="312.42" y="210.82" smashed="yes" rot="R270">
<attribute name="NAME" x="310.388" y="207.264" size="1.778" layer="95"/>
<attribute name="VALUE" x="310.388" y="215.265" size="1.778" layer="96"/>
</instance>
<instance part="D10" gate="G$1" x="312.42" y="223.52" smashed="yes" rot="R270">
<attribute name="NAME" x="310.388" y="219.964" size="1.778" layer="95"/>
<attribute name="VALUE" x="307.848" y="227.965" size="1.778" layer="96"/>
</instance>
<instance part="R13" gate="G$1" x="302.26" y="160.02" smashed="yes">
<attribute name="NAME" x="298.45" y="161.5186" size="1.778" layer="95"/>
<attribute name="VALUE" x="298.45" y="156.718" size="1.778" layer="96"/>
<attribute name="PRECISION" x="298.45" y="153.162" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="298.45" y="154.94" size="1.27" layer="97"/>
</instance>
<instance part="R14" gate="G$1" x="302.26" y="172.72" smashed="yes">
<attribute name="NAME" x="298.45" y="174.2186" size="1.778" layer="95"/>
<attribute name="VALUE" x="298.45" y="169.418" size="1.778" layer="96"/>
<attribute name="PRECISION" x="298.45" y="165.862" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="298.45" y="167.64" size="1.27" layer="97"/>
</instance>
<instance part="U$8" gate="G$1" x="365.76" y="182.88" smashed="yes">
<attribute name="NAME" x="363.22" y="200.66" size="1.27" layer="95"/>
<attribute name="VALUE" x="363.22" y="154.94" size="1.27" layer="96"/>
</instance>
<instance part="+3V4" gate="G$1" x="320.04" y="198.12" smashed="yes" rot="R90">
<attribute name="VALUE" x="325.12" y="195.58" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND20" gate="1" x="320.04" y="190.5" smashed="yes" rot="R270">
<attribute name="VALUE" x="317.5" y="193.04" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="U$9" gate="G$1" x="393.7" y="190.5" smashed="yes">
<attribute name="VALUE" x="388.62" y="178.054" size="1.778" layer="96" font="vector"/>
<attribute name="NAME" x="388.62" y="201.168" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="C19" gate="G$1" x="327.66" y="193.04" smashed="yes">
<attribute name="NAME" x="329.184" y="195.961" size="1.778" layer="95"/>
<attribute name="VALUE" x="329.184" y="190.881" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="329.184" y="188.976" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="329.184" y="187.198" size="1.27" layer="97"/>
<attribute name="TYPE" x="329.184" y="185.42" size="1.27" layer="97"/>
</instance>
<instance part="D11" gate="G$1" x="312.42" y="160.02" smashed="yes" rot="R270">
<attribute name="NAME" x="310.388" y="156.464" size="1.778" layer="95"/>
<attribute name="VALUE" x="310.388" y="164.465" size="1.778" layer="96"/>
</instance>
<instance part="D12" gate="G$1" x="312.42" y="172.72" smashed="yes" rot="R270">
<attribute name="NAME" x="310.388" y="169.164" size="1.778" layer="95"/>
<attribute name="VALUE" x="307.848" y="177.165" size="1.778" layer="96"/>
</instance>
<instance part="R15" gate="G$1" x="302.26" y="109.22" smashed="yes">
<attribute name="NAME" x="298.45" y="110.7186" size="1.778" layer="95"/>
<attribute name="VALUE" x="298.45" y="105.918" size="1.778" layer="96"/>
<attribute name="PRECISION" x="298.45" y="102.362" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="298.45" y="104.14" size="1.27" layer="97"/>
</instance>
<instance part="R16" gate="G$1" x="302.26" y="121.92" smashed="yes">
<attribute name="NAME" x="298.45" y="123.4186" size="1.778" layer="95"/>
<attribute name="VALUE" x="298.45" y="118.618" size="1.778" layer="96"/>
<attribute name="PRECISION" x="298.45" y="115.062" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="298.45" y="116.84" size="1.27" layer="97"/>
</instance>
<instance part="U$10" gate="G$1" x="365.76" y="132.08" smashed="yes">
<attribute name="NAME" x="363.22" y="149.86" size="1.27" layer="95"/>
<attribute name="VALUE" x="363.22" y="104.14" size="1.27" layer="96"/>
</instance>
<instance part="+3V8" gate="G$1" x="320.04" y="147.32" smashed="yes" rot="R90">
<attribute name="VALUE" x="325.12" y="144.78" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND23" gate="1" x="320.04" y="139.7" smashed="yes" rot="R270">
<attribute name="VALUE" x="317.5" y="142.24" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="U$11" gate="G$1" x="393.7" y="139.7" smashed="yes">
<attribute name="VALUE" x="388.62" y="127.254" size="1.778" layer="96" font="vector"/>
<attribute name="NAME" x="388.62" y="150.368" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="C20" gate="G$1" x="327.66" y="142.24" smashed="yes">
<attribute name="NAME" x="329.184" y="145.161" size="1.778" layer="95"/>
<attribute name="VALUE" x="329.184" y="140.081" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="329.184" y="138.176" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="329.184" y="136.398" size="1.27" layer="97"/>
<attribute name="TYPE" x="329.184" y="134.62" size="1.27" layer="97"/>
</instance>
<instance part="D13" gate="G$1" x="312.42" y="109.22" smashed="yes" rot="R270">
<attribute name="NAME" x="310.388" y="105.664" size="1.778" layer="95"/>
<attribute name="VALUE" x="310.388" y="113.665" size="1.778" layer="96"/>
</instance>
<instance part="D14" gate="G$1" x="312.42" y="121.92" smashed="yes" rot="R270">
<attribute name="NAME" x="310.388" y="118.364" size="1.778" layer="95"/>
<attribute name="VALUE" x="307.848" y="126.365" size="1.778" layer="96"/>
</instance>
<instance part="U$12" gate="G$1" x="365.76" y="30.48" smashed="yes">
<attribute name="NAME" x="363.22" y="48.26" size="1.27" layer="95"/>
<attribute name="VALUE" x="363.22" y="2.54" size="1.27" layer="96"/>
</instance>
<instance part="+3V9" gate="G$1" x="320.04" y="45.72" smashed="yes" rot="R90">
<attribute name="VALUE" x="325.12" y="43.18" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND26" gate="1" x="320.04" y="38.1" smashed="yes" rot="R270">
<attribute name="VALUE" x="317.5" y="40.64" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="U$13" gate="G$1" x="393.7" y="38.1" smashed="yes">
<attribute name="VALUE" x="388.62" y="25.654" size="1.778" layer="96" font="vector"/>
<attribute name="NAME" x="388.62" y="48.768" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="C21" gate="G$1" x="327.66" y="40.64" smashed="yes">
<attribute name="NAME" x="329.184" y="43.561" size="1.778" layer="95"/>
<attribute name="VALUE" x="329.184" y="38.481" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="329.184" y="36.576" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="329.184" y="34.798" size="1.27" layer="97"/>
<attribute name="TYPE" x="329.184" y="33.02" size="1.27" layer="97"/>
</instance>
<instance part="R17" gate="G$1" x="302.26" y="7.62" smashed="yes">
<attribute name="NAME" x="298.45" y="9.1186" size="1.778" layer="95"/>
<attribute name="VALUE" x="298.45" y="4.318" size="1.778" layer="96"/>
<attribute name="PRECISION" x="298.45" y="0.762" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="298.45" y="2.54" size="1.27" layer="97"/>
</instance>
<instance part="R18" gate="G$1" x="302.26" y="20.32" smashed="yes">
<attribute name="NAME" x="298.45" y="21.8186" size="1.778" layer="95"/>
<attribute name="VALUE" x="298.45" y="17.018" size="1.778" layer="96"/>
<attribute name="PRECISION" x="298.45" y="13.462" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="298.45" y="15.24" size="1.27" layer="97"/>
</instance>
<instance part="D15" gate="G$1" x="312.42" y="7.62" smashed="yes" rot="R270">
<attribute name="NAME" x="310.388" y="4.064" size="1.778" layer="95"/>
<attribute name="VALUE" x="310.388" y="12.065" size="1.778" layer="96"/>
</instance>
<instance part="D16" gate="G$1" x="312.42" y="20.32" smashed="yes" rot="R270">
<attribute name="NAME" x="310.388" y="16.764" size="1.778" layer="95"/>
<attribute name="VALUE" x="307.848" y="24.765" size="1.778" layer="96"/>
</instance>
<instance part="+3V10" gate="G$1" x="325.12" y="20.32" smashed="yes" rot="R270">
<attribute name="VALUE" x="320.04" y="22.86" size="1.778" layer="96"/>
</instance>
<instance part="+3V11" gate="G$1" x="325.12" y="7.62" smashed="yes" rot="R270">
<attribute name="VALUE" x="320.04" y="10.16" size="1.778" layer="96"/>
</instance>
<instance part="+3V12" gate="G$1" x="325.12" y="58.42" smashed="yes" rot="R270">
<attribute name="VALUE" x="320.04" y="60.96" size="1.778" layer="96"/>
</instance>
<instance part="+3V13" gate="G$1" x="325.12" y="71.12" smashed="yes" rot="R270">
<attribute name="VALUE" x="320.04" y="73.66" size="1.778" layer="96"/>
</instance>
<instance part="+3V14" gate="G$1" x="325.12" y="109.22" smashed="yes" rot="R270">
<attribute name="VALUE" x="320.04" y="111.76" size="1.778" layer="96"/>
</instance>
<instance part="+3V15" gate="G$1" x="325.12" y="121.92" smashed="yes" rot="R270">
<attribute name="VALUE" x="320.04" y="124.46" size="1.778" layer="96"/>
</instance>
<instance part="+3V16" gate="G$1" x="325.12" y="160.02" smashed="yes" rot="R270">
<attribute name="VALUE" x="320.04" y="162.56" size="1.778" layer="96"/>
</instance>
<instance part="+3V17" gate="G$1" x="325.12" y="172.72" smashed="yes" rot="R270">
<attribute name="VALUE" x="320.04" y="175.26" size="1.778" layer="96"/>
</instance>
<instance part="+3V18" gate="G$1" x="325.12" y="210.82" smashed="yes" rot="R270">
<attribute name="VALUE" x="320.04" y="213.36" size="1.778" layer="96"/>
</instance>
<instance part="+3V19" gate="G$1" x="325.12" y="223.52" smashed="yes" rot="R270">
<attribute name="VALUE" x="320.04" y="226.06" size="1.778" layer="96"/>
</instance>
<instance part="+3V20" gate="G$1" x="325.12" y="261.62" smashed="yes" rot="R270">
<attribute name="VALUE" x="320.04" y="264.16" size="1.778" layer="96"/>
</instance>
<instance part="+3V21" gate="G$1" x="325.12" y="274.32" smashed="yes" rot="R270">
<attribute name="VALUE" x="320.04" y="276.86" size="1.778" layer="96"/>
</instance>
<instance part="U$14" gate="G$1" x="96.52" y="337.82" smashed="yes">
<attribute name="VALUE" x="93.98" y="332.994" size="1.778" layer="96" font="vector"/>
<attribute name="NAME" x="93.98" y="340.868" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="U$15" gate="G$1" x="96.52" y="322.58" smashed="yes">
<attribute name="VALUE" x="93.98" y="317.754" size="1.778" layer="96" font="vector"/>
<attribute name="NAME" x="93.98" y="325.628" size="1.778" layer="95" font="vector"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="172.72" y1="396.24" x2="162.56" y2="396.24" width="0.1524" layer="91"/>
<label x="162.56" y="396.24" size="1.778" layer="95"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="GND"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="177.8" y1="325.12" x2="177.8" y2="322.58" width="0.1524" layer="91"/>
<wire x1="177.8" y1="322.58" x2="177.8" y2="320.04" width="0.1524" layer="91"/>
<wire x1="177.8" y1="322.58" x2="193.04" y2="322.58" width="0.1524" layer="91"/>
<junction x="177.8" y="322.58"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="193.04" y1="322.58" x2="193.04" y2="330.2" width="0.1524" layer="91"/>
<junction x="193.04" y="322.58"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="205.74" y1="322.58" x2="193.04" y2="322.58" width="0.1524" layer="91"/>
<wire x1="205.74" y1="330.2" x2="205.74" y2="322.58" width="0.1524" layer="91"/>
<wire x1="104.14" y1="322.58" x2="177.8" y2="322.58" width="0.1524" layer="91"/>
<pinref part="U$15" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="GND"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="149.86" y1="358.14" x2="165.1" y2="358.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="96.52" y1="157.48" x2="96.52" y2="165.1" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="GND"/>
<wire x1="96.52" y1="165.1" x2="104.14" y2="165.1" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="78.74" y1="167.64" x2="78.74" y2="165.1" width="0.1524" layer="91"/>
<wire x1="78.74" y1="165.1" x2="96.52" y2="165.1" width="0.1524" layer="91"/>
<junction x="96.52" y="165.1"/>
<pinref part="S1" gate="G$1" pin="P1"/>
<wire x1="71.12" y1="165.1" x2="78.74" y2="165.1" width="0.1524" layer="91"/>
<junction x="78.74" y="165.1"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="78.74" y1="243.84" x2="78.74" y2="241.3" width="0.1524" layer="91"/>
<wire x1="78.74" y1="241.3" x2="78.74" y2="236.22" width="0.1524" layer="91"/>
<wire x1="78.74" y1="241.3" x2="91.44" y2="241.3" width="0.1524" layer="91"/>
<junction x="78.74" y="241.3"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="91.44" y1="241.3" x2="91.44" y2="243.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="78.74" y1="289.56" x2="78.74" y2="287.02" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="66.04" y1="289.56" x2="66.04" y2="287.02" width="0.1524" layer="91"/>
<wire x1="78.74" y1="287.02" x2="66.04" y2="287.02" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="53.34" y1="289.56" x2="53.34" y2="287.02" width="0.1524" layer="91"/>
<wire x1="66.04" y1="287.02" x2="53.34" y2="287.02" width="0.1524" layer="91"/>
<junction x="66.04" y="287.02"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="40.64" y1="289.56" x2="40.64" y2="287.02" width="0.1524" layer="91"/>
<wire x1="53.34" y1="287.02" x2="40.64" y2="287.02" width="0.1524" layer="91"/>
<junction x="53.34" y="287.02"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="40.64" y1="281.94" x2="40.64" y2="287.02" width="0.1524" layer="91"/>
<junction x="40.64" y="287.02"/>
<wire x1="78.74" y1="287.02" x2="91.44" y2="287.02" width="0.1524" layer="91"/>
<junction x="78.74" y="287.02"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="91.44" y1="287.02" x2="91.44" y2="289.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="322.58" y1="88.9" x2="327.66" y2="88.9" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="!RE!"/>
<wire x1="327.66" y1="88.9" x2="342.9" y2="88.9" width="0.1524" layer="91"/>
<wire x1="342.9" y1="88.9" x2="347.98" y2="88.9" width="0.1524" layer="91"/>
<wire x1="347.98" y1="76.2" x2="342.9" y2="76.2" width="0.1524" layer="91"/>
<wire x1="342.9" y1="76.2" x2="342.9" y2="88.9" width="0.1524" layer="91"/>
<junction x="342.9" y="88.9"/>
<pinref part="C15" gate="G$1" pin="2"/>
<junction x="327.66" y="88.9"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="GND"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="322.58" y1="292.1" x2="327.66" y2="292.1" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="!RE!"/>
<wire x1="327.66" y1="292.1" x2="342.9" y2="292.1" width="0.1524" layer="91"/>
<wire x1="342.9" y1="292.1" x2="347.98" y2="292.1" width="0.1524" layer="91"/>
<wire x1="347.98" y1="279.4" x2="342.9" y2="279.4" width="0.1524" layer="91"/>
<wire x1="342.9" y1="279.4" x2="342.9" y2="292.1" width="0.1524" layer="91"/>
<junction x="342.9" y="292.1"/>
<pinref part="C16" gate="G$1" pin="2"/>
<junction x="327.66" y="292.1"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="241.3" y1="238.76" x2="248.92" y2="238.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J6" gate="G$1" pin="GND"/>
<wire x1="25.4" y1="193.04" x2="7.62" y2="193.04" width="0.1524" layer="91"/>
<wire x1="7.62" y1="193.04" x2="7.62" y2="182.88" width="0.1524" layer="91"/>
<pinref part="GND13" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="236.22" y1="299.72" x2="238.76" y2="299.72" width="0.1524" layer="91"/>
<wire x1="238.76" y1="299.72" x2="238.76" y2="297.18" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="238.76" y1="297.18" x2="238.76" y2="294.64" width="0.1524" layer="91"/>
<wire x1="238.76" y1="294.64" x2="236.22" y2="294.64" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<junction x="238.76" y="297.18"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="236.22" y1="264.16" x2="238.76" y2="264.16" width="0.1524" layer="91"/>
<wire x1="238.76" y1="264.16" x2="238.76" y2="261.62" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="238.76" y1="261.62" x2="238.76" y2="259.08" width="0.1524" layer="91"/>
<wire x1="238.76" y1="259.08" x2="236.22" y2="259.08" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
<junction x="238.76" y="261.62"/>
<pinref part="Y1" gate="G$1" pin="GND"/>
<wire x1="228.6" y1="261.62" x2="238.76" y2="261.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="GND"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="322.58" y1="241.3" x2="327.66" y2="241.3" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="!RE!"/>
<wire x1="327.66" y1="241.3" x2="342.9" y2="241.3" width="0.1524" layer="91"/>
<wire x1="342.9" y1="241.3" x2="347.98" y2="241.3" width="0.1524" layer="91"/>
<wire x1="347.98" y1="228.6" x2="342.9" y2="228.6" width="0.1524" layer="91"/>
<wire x1="342.9" y1="228.6" x2="342.9" y2="241.3" width="0.1524" layer="91"/>
<junction x="342.9" y="241.3"/>
<pinref part="C18" gate="G$1" pin="2"/>
<junction x="327.66" y="241.3"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="GND"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="322.58" y1="190.5" x2="327.66" y2="190.5" width="0.1524" layer="91"/>
<pinref part="U$8" gate="G$1" pin="!RE!"/>
<wire x1="327.66" y1="190.5" x2="342.9" y2="190.5" width="0.1524" layer="91"/>
<wire x1="342.9" y1="190.5" x2="347.98" y2="190.5" width="0.1524" layer="91"/>
<wire x1="347.98" y1="177.8" x2="342.9" y2="177.8" width="0.1524" layer="91"/>
<wire x1="342.9" y1="177.8" x2="342.9" y2="190.5" width="0.1524" layer="91"/>
<junction x="342.9" y="190.5"/>
<pinref part="C19" gate="G$1" pin="2"/>
<junction x="327.66" y="190.5"/>
</segment>
<segment>
<pinref part="U$10" gate="G$1" pin="GND"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="322.58" y1="139.7" x2="327.66" y2="139.7" width="0.1524" layer="91"/>
<pinref part="U$10" gate="G$1" pin="!RE!"/>
<wire x1="327.66" y1="139.7" x2="342.9" y2="139.7" width="0.1524" layer="91"/>
<wire x1="342.9" y1="139.7" x2="347.98" y2="139.7" width="0.1524" layer="91"/>
<wire x1="347.98" y1="127" x2="342.9" y2="127" width="0.1524" layer="91"/>
<wire x1="342.9" y1="127" x2="342.9" y2="139.7" width="0.1524" layer="91"/>
<junction x="342.9" y="139.7"/>
<pinref part="C20" gate="G$1" pin="2"/>
<junction x="327.66" y="139.7"/>
</segment>
<segment>
<pinref part="U$12" gate="G$1" pin="GND"/>
<pinref part="GND26" gate="1" pin="GND"/>
<wire x1="322.58" y1="38.1" x2="327.66" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="!RE!"/>
<wire x1="327.66" y1="38.1" x2="342.9" y2="38.1" width="0.1524" layer="91"/>
<wire x1="342.9" y1="38.1" x2="347.98" y2="38.1" width="0.1524" layer="91"/>
<wire x1="347.98" y1="25.4" x2="342.9" y2="25.4" width="0.1524" layer="91"/>
<wire x1="342.9" y1="25.4" x2="342.9" y2="38.1" width="0.1524" layer="91"/>
<junction x="342.9" y="38.1"/>
<pinref part="C21" gate="G$1" pin="2"/>
<junction x="327.66" y="38.1"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="195.58" y1="396.24" x2="205.74" y2="396.24" width="0.1524" layer="91"/>
<wire x1="205.74" y1="396.24" x2="205.74" y2="386.08" width="0.1524" layer="91"/>
<wire x1="205.74" y1="386.08" x2="205.74" y2="381" width="0.1524" layer="91"/>
<wire x1="205.74" y1="381" x2="205.74" y2="375.92" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="195.58" y1="375.92" x2="205.74" y2="375.92" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="195.58" y1="386.08" x2="205.74" y2="386.08" width="0.1524" layer="91"/>
<junction x="205.74" y="386.08"/>
<wire x1="205.74" y1="381" x2="220.98" y2="381" width="0.1524" layer="91"/>
<junction x="205.74" y="381"/>
<pinref part="+3V33" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VOUT"/>
<pinref part="+3V34" gate="G$1" pin="+3V3"/>
<wire x1="190.5" y1="337.82" x2="193.04" y2="337.82" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="1"/>
<junction x="193.04" y="337.82"/>
<wire x1="193.04" y1="337.82" x2="205.74" y2="337.82" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="205.74" y1="337.82" x2="220.98" y2="337.82" width="0.1524" layer="91"/>
<junction x="205.74" y="337.82"/>
</segment>
<segment>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="78.74" y1="213.36" x2="78.74" y2="208.28" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="78.74" y1="208.28" x2="78.74" y2="195.58" width="0.1524" layer="91"/>
<wire x1="71.12" y1="205.74" x2="71.12" y2="208.28" width="0.1524" layer="91"/>
<wire x1="71.12" y1="208.28" x2="78.74" y2="208.28" width="0.1524" layer="91"/>
<junction x="78.74" y="208.28"/>
</segment>
<segment>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<pinref part="U5" gate="G$1" pin="VDDIO"/>
<wire x1="33.02" y1="297.18" x2="40.64" y2="297.18" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="VDDANA"/>
<wire x1="40.64" y1="297.18" x2="53.34" y2="297.18" width="0.1524" layer="91"/>
<wire x1="53.34" y1="297.18" x2="66.04" y2="297.18" width="0.1524" layer="91"/>
<wire x1="66.04" y1="297.18" x2="78.74" y2="297.18" width="0.1524" layer="91"/>
<wire x1="78.74" y1="297.18" x2="91.44" y2="297.18" width="0.1524" layer="91"/>
<wire x1="91.44" y1="297.18" x2="101.6" y2="297.18" width="0.1524" layer="91"/>
<wire x1="101.6" y1="297.18" x2="104.14" y2="297.18" width="0.1524" layer="91"/>
<wire x1="104.14" y1="281.94" x2="101.6" y2="281.94" width="0.1524" layer="91"/>
<wire x1="101.6" y1="281.94" x2="101.6" y2="297.18" width="0.1524" layer="91"/>
<junction x="101.6" y="297.18"/>
<pinref part="C5" gate="G$1" pin="1"/>
<junction x="78.74" y="297.18"/>
<pinref part="C6" gate="G$1" pin="1"/>
<junction x="66.04" y="297.18"/>
<pinref part="C7" gate="G$1" pin="1"/>
<junction x="53.34" y="297.18"/>
<pinref part="C8" gate="G$1" pin="1"/>
<junction x="40.64" y="297.18"/>
<pinref part="C17" gate="G$1" pin="1"/>
<junction x="91.44" y="297.18"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="VCC"/>
<pinref part="+3V5" gate="G$1" pin="+3V3"/>
<wire x1="322.58" y1="96.52" x2="327.66" y2="96.52" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="TE"/>
<wire x1="327.66" y1="96.52" x2="340.36" y2="96.52" width="0.1524" layer="91"/>
<wire x1="340.36" y1="96.52" x2="345.44" y2="96.52" width="0.1524" layer="91"/>
<wire x1="345.44" y1="96.52" x2="347.98" y2="96.52" width="0.1524" layer="91"/>
<wire x1="347.98" y1="81.28" x2="345.44" y2="81.28" width="0.1524" layer="91"/>
<wire x1="345.44" y1="81.28" x2="345.44" y2="96.52" width="0.1524" layer="91"/>
<junction x="345.44" y="96.52"/>
<pinref part="U$1" gate="G$1" pin="DE"/>
<wire x1="347.98" y1="63.5" x2="340.36" y2="63.5" width="0.1524" layer="91"/>
<wire x1="340.36" y1="63.5" x2="340.36" y2="96.52" width="0.1524" layer="91"/>
<junction x="340.36" y="96.52"/>
<pinref part="C15" gate="G$1" pin="1"/>
<junction x="327.66" y="96.52"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="VCC"/>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
<wire x1="322.58" y1="299.72" x2="327.66" y2="299.72" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="TE"/>
<wire x1="327.66" y1="299.72" x2="340.36" y2="299.72" width="0.1524" layer="91"/>
<wire x1="340.36" y1="299.72" x2="345.44" y2="299.72" width="0.1524" layer="91"/>
<wire x1="345.44" y1="299.72" x2="347.98" y2="299.72" width="0.1524" layer="91"/>
<wire x1="347.98" y1="284.48" x2="345.44" y2="284.48" width="0.1524" layer="91"/>
<wire x1="345.44" y1="284.48" x2="345.44" y2="299.72" width="0.1524" layer="91"/>
<junction x="345.44" y="299.72"/>
<pinref part="U$2" gate="G$1" pin="DE"/>
<wire x1="347.98" y1="266.7" x2="340.36" y2="266.7" width="0.1524" layer="91"/>
<wire x1="340.36" y1="266.7" x2="340.36" y2="299.72" width="0.1524" layer="91"/>
<junction x="340.36" y="299.72"/>
<pinref part="C16" gate="G$1" pin="1"/>
<junction x="327.66" y="299.72"/>
</segment>
<segment>
<pinref part="J6" gate="G$1" pin="VDD"/>
<wire x1="25.4" y1="198.12" x2="7.62" y2="198.12" width="0.1524" layer="91"/>
<wire x1="7.62" y1="198.12" x2="7.62" y2="203.2" width="0.1524" layer="91"/>
<pinref part="+3V7" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="VCC"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="322.58" y1="248.92" x2="327.66" y2="248.92" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="TE"/>
<wire x1="327.66" y1="248.92" x2="340.36" y2="248.92" width="0.1524" layer="91"/>
<wire x1="340.36" y1="248.92" x2="345.44" y2="248.92" width="0.1524" layer="91"/>
<wire x1="345.44" y1="248.92" x2="347.98" y2="248.92" width="0.1524" layer="91"/>
<wire x1="347.98" y1="233.68" x2="345.44" y2="233.68" width="0.1524" layer="91"/>
<wire x1="345.44" y1="233.68" x2="345.44" y2="248.92" width="0.1524" layer="91"/>
<junction x="345.44" y="248.92"/>
<pinref part="U$6" gate="G$1" pin="DE"/>
<wire x1="347.98" y1="215.9" x2="340.36" y2="215.9" width="0.1524" layer="91"/>
<wire x1="340.36" y1="215.9" x2="340.36" y2="248.92" width="0.1524" layer="91"/>
<junction x="340.36" y="248.92"/>
<pinref part="C18" gate="G$1" pin="1"/>
<junction x="327.66" y="248.92"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="VCC"/>
<pinref part="+3V4" gate="G$1" pin="+3V3"/>
<wire x1="322.58" y1="198.12" x2="327.66" y2="198.12" width="0.1524" layer="91"/>
<pinref part="U$8" gate="G$1" pin="TE"/>
<wire x1="327.66" y1="198.12" x2="340.36" y2="198.12" width="0.1524" layer="91"/>
<wire x1="340.36" y1="198.12" x2="345.44" y2="198.12" width="0.1524" layer="91"/>
<wire x1="345.44" y1="198.12" x2="347.98" y2="198.12" width="0.1524" layer="91"/>
<wire x1="347.98" y1="182.88" x2="345.44" y2="182.88" width="0.1524" layer="91"/>
<wire x1="345.44" y1="182.88" x2="345.44" y2="198.12" width="0.1524" layer="91"/>
<junction x="345.44" y="198.12"/>
<pinref part="U$8" gate="G$1" pin="DE"/>
<wire x1="347.98" y1="165.1" x2="340.36" y2="165.1" width="0.1524" layer="91"/>
<wire x1="340.36" y1="165.1" x2="340.36" y2="198.12" width="0.1524" layer="91"/>
<junction x="340.36" y="198.12"/>
<pinref part="C19" gate="G$1" pin="1"/>
<junction x="327.66" y="198.12"/>
</segment>
<segment>
<pinref part="U$10" gate="G$1" pin="VCC"/>
<pinref part="+3V8" gate="G$1" pin="+3V3"/>
<wire x1="322.58" y1="147.32" x2="327.66" y2="147.32" width="0.1524" layer="91"/>
<pinref part="U$10" gate="G$1" pin="TE"/>
<wire x1="327.66" y1="147.32" x2="340.36" y2="147.32" width="0.1524" layer="91"/>
<wire x1="340.36" y1="147.32" x2="345.44" y2="147.32" width="0.1524" layer="91"/>
<wire x1="345.44" y1="147.32" x2="347.98" y2="147.32" width="0.1524" layer="91"/>
<wire x1="347.98" y1="132.08" x2="345.44" y2="132.08" width="0.1524" layer="91"/>
<wire x1="345.44" y1="132.08" x2="345.44" y2="147.32" width="0.1524" layer="91"/>
<junction x="345.44" y="147.32"/>
<pinref part="U$10" gate="G$1" pin="DE"/>
<wire x1="347.98" y1="114.3" x2="340.36" y2="114.3" width="0.1524" layer="91"/>
<wire x1="340.36" y1="114.3" x2="340.36" y2="147.32" width="0.1524" layer="91"/>
<junction x="340.36" y="147.32"/>
<pinref part="C20" gate="G$1" pin="1"/>
<junction x="327.66" y="147.32"/>
</segment>
<segment>
<pinref part="U$12" gate="G$1" pin="VCC"/>
<pinref part="+3V9" gate="G$1" pin="+3V3"/>
<wire x1="322.58" y1="45.72" x2="327.66" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="TE"/>
<wire x1="327.66" y1="45.72" x2="340.36" y2="45.72" width="0.1524" layer="91"/>
<wire x1="340.36" y1="45.72" x2="345.44" y2="45.72" width="0.1524" layer="91"/>
<wire x1="345.44" y1="45.72" x2="347.98" y2="45.72" width="0.1524" layer="91"/>
<wire x1="347.98" y1="30.48" x2="345.44" y2="30.48" width="0.1524" layer="91"/>
<wire x1="345.44" y1="30.48" x2="345.44" y2="45.72" width="0.1524" layer="91"/>
<junction x="345.44" y="45.72"/>
<pinref part="U$12" gate="G$1" pin="DE"/>
<wire x1="347.98" y1="12.7" x2="340.36" y2="12.7" width="0.1524" layer="91"/>
<wire x1="340.36" y1="12.7" x2="340.36" y2="45.72" width="0.1524" layer="91"/>
<junction x="340.36" y="45.72"/>
<pinref part="C21" gate="G$1" pin="1"/>
<junction x="327.66" y="45.72"/>
</segment>
<segment>
<pinref part="D16" gate="G$1" pin="A"/>
<wire x1="322.58" y1="20.32" x2="317.5" y2="20.32" width="0.1524" layer="91"/>
<pinref part="+3V10" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="D15" gate="G$1" pin="A"/>
<wire x1="322.58" y1="7.62" x2="317.5" y2="7.62" width="0.1524" layer="91"/>
<pinref part="+3V11" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="D7" gate="G$1" pin="A"/>
<wire x1="322.58" y1="58.42" x2="317.5" y2="58.42" width="0.1524" layer="91"/>
<pinref part="+3V12" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="D8" gate="G$1" pin="A"/>
<wire x1="322.58" y1="71.12" x2="317.5" y2="71.12" width="0.1524" layer="91"/>
<pinref part="+3V13" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="D13" gate="G$1" pin="A"/>
<wire x1="322.58" y1="109.22" x2="317.5" y2="109.22" width="0.1524" layer="91"/>
<pinref part="+3V14" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="D14" gate="G$1" pin="A"/>
<wire x1="322.58" y1="121.92" x2="317.5" y2="121.92" width="0.1524" layer="91"/>
<pinref part="+3V15" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="D11" gate="G$1" pin="A"/>
<wire x1="322.58" y1="160.02" x2="317.5" y2="160.02" width="0.1524" layer="91"/>
<pinref part="+3V16" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="D12" gate="G$1" pin="A"/>
<wire x1="322.58" y1="172.72" x2="317.5" y2="172.72" width="0.1524" layer="91"/>
<pinref part="+3V17" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="D9" gate="G$1" pin="A"/>
<wire x1="322.58" y1="210.82" x2="317.5" y2="210.82" width="0.1524" layer="91"/>
<pinref part="+3V18" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="D10" gate="G$1" pin="A"/>
<wire x1="322.58" y1="223.52" x2="317.5" y2="223.52" width="0.1524" layer="91"/>
<pinref part="+3V19" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="D5" gate="G$1" pin="A"/>
<wire x1="322.58" y1="261.62" x2="317.5" y2="261.62" width="0.1524" layer="91"/>
<pinref part="+3V20" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="D6" gate="G$1" pin="A"/>
<wire x1="322.58" y1="274.32" x2="317.5" y2="274.32" width="0.1524" layer="91"/>
<pinref part="+3V21" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="182.88" y1="396.24" x2="187.96" y2="396.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="182.88" y1="386.08" x2="187.96" y2="386.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="D3" gate="G$1" pin="C"/>
<wire x1="182.88" y1="375.92" x2="187.96" y2="375.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="STLCLK" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="172.72" y1="386.08" x2="162.56" y2="386.08" width="0.1524" layer="91"/>
<label x="162.56" y="386.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PA21/SER5-3/SER3-3/TC7-1"/>
<wire x1="182.88" y1="243.84" x2="208.28" y2="243.84" width="0.1524" layer="91"/>
<label x="185.42" y="243.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="STLERR" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="172.72" y1="375.92" x2="162.56" y2="375.92" width="0.1524" layer="91"/>
<label x="162.56" y="375.92" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PB23/XOUT1/SER1-3/SER5-3/TC7-1"/>
<wire x1="182.88" y1="170.18" x2="208.28" y2="170.18" width="0.1524" layer="91"/>
<label x="185.42" y="170.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="P+5" gate="1" pin="+5V"/>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="200.66" y1="355.6" x2="187.96" y2="355.6" width="0.1524" layer="91"/>
<wire x1="187.96" y1="355.6" x2="187.96" y2="360.68" width="0.1524" layer="91"/>
<junction x="187.96" y="355.6"/>
<wire x1="187.96" y1="360.68" x2="177.8" y2="360.68" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="2"/>
<pinref part="X1" gate="G$1" pin="VBUS"/>
<wire x1="177.8" y1="355.6" x2="149.86" y2="355.6" width="0.1524" layer="91"/>
<wire x1="177.8" y1="360.68" x2="177.8" y2="355.6" width="0.1524" layer="91"/>
<junction x="177.8" y="355.6"/>
</segment>
<segment>
<pinref part="P+2" gate="1" pin="+5V"/>
<pinref part="U3" gate="G$1" pin="VIN"/>
<pinref part="U3" gate="G$1" pin="EN"/>
<wire x1="162.56" y1="337.82" x2="165.1" y2="337.82" width="0.1524" layer="91"/>
<wire x1="165.1" y1="332.74" x2="162.56" y2="332.74" width="0.1524" layer="91"/>
<wire x1="162.56" y1="332.74" x2="162.56" y2="337.82" width="0.1524" layer="91"/>
<junction x="162.56" y="337.82"/>
<wire x1="104.14" y1="337.82" x2="132.08" y2="337.82" width="0.1524" layer="91"/>
<wire x1="132.08" y1="337.82" x2="162.56" y2="337.82" width="0.1524" layer="91"/>
<wire x1="132.08" y1="340.36" x2="132.08" y2="337.82" width="0.1524" layer="91"/>
<junction x="132.08" y="337.82"/>
<pinref part="U$14" gate="G$1" pin="2"/>
</segment>
</net>
<net name="USBDM" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="D-"/>
<wire x1="149.86" y1="353.06" x2="165.1" y2="353.06" width="0.1524" layer="91"/>
<label x="165.1" y="353.06" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PA24/SER3-2/SER5-2/TC5-0/PDEC0-0/USBDM"/>
<wire x1="182.88" y1="236.22" x2="208.28" y2="236.22" width="0.1524" layer="91"/>
<label x="185.42" y="236.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="USBDP" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="D+"/>
<wire x1="149.86" y1="350.52" x2="165.1" y2="350.52" width="0.1524" layer="91"/>
<label x="165.1" y="350.52" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PA25/SER3-3/SER5-3/TC5-1/PDEC0-1/USBDP"/>
<wire x1="182.88" y1="233.68" x2="208.28" y2="233.68" width="0.1524" layer="91"/>
<label x="185.42" y="233.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="SWDIO" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="PA31/SER7-3/SER1-3/TC6-1/SWDIO"/>
<wire x1="182.88" y1="226.06" x2="208.28" y2="226.06" width="0.1524" layer="91"/>
<label x="185.42" y="226.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J6" gate="G$1" pin="SWDIO"/>
<wire x1="25.4" y1="185.42" x2="12.7" y2="185.42" width="0.1524" layer="91"/>
<label x="12.7" y="185.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="SWDCLK" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="71.12" y1="190.5" x2="71.12" y2="195.58" width="0.1524" layer="91"/>
<wire x1="71.12" y1="190.5" x2="55.88" y2="190.5" width="0.1524" layer="91"/>
<label x="55.88" y="190.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PA30/SER7-2/SER1-2/TC6-0/SWCLK"/>
<wire x1="182.88" y1="228.6" x2="208.28" y2="228.6" width="0.1524" layer="91"/>
<label x="185.42" y="228.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J6" gate="G$1" pin="SWCLK"/>
<wire x1="25.4" y1="187.96" x2="12.7" y2="187.96" width="0.1524" layer="91"/>
<label x="12.7" y="187.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<wire x1="55.88" y1="182.88" x2="60.96" y2="182.88" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="60.96" y1="182.88" x2="78.74" y2="182.88" width="0.1524" layer="91"/>
<wire x1="78.74" y1="182.88" x2="78.74" y2="185.42" width="0.1524" layer="91"/>
<junction x="78.74" y="182.88"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="78.74" y1="175.26" x2="78.74" y2="177.8" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="RESETN"/>
<wire x1="78.74" y1="177.8" x2="78.74" y2="182.88" width="0.1524" layer="91"/>
<wire x1="78.74" y1="177.8" x2="104.14" y2="177.8" width="0.1524" layer="91"/>
<junction x="78.74" y="177.8"/>
<label x="86.36" y="177.8" size="1.778" layer="95"/>
<pinref part="S1" gate="G$1" pin="S1"/>
<wire x1="60.96" y1="165.1" x2="60.96" y2="182.88" width="0.1524" layer="91"/>
<junction x="60.96" y="182.88"/>
</segment>
<segment>
<pinref part="J6" gate="G$1" pin="RESET"/>
<wire x1="25.4" y1="195.58" x2="12.7" y2="195.58" width="0.1524" layer="91"/>
<label x="12.7" y="195.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="PORTA-TX" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="PA22/SER3-0/SER5-1/TC4-0"/>
<wire x1="182.88" y1="241.3" x2="208.28" y2="241.3" width="0.1524" layer="91"/>
<label x="185.42" y="241.3" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="DI"/>
<wire x1="347.98" y1="261.62" x2="335.28" y2="261.62" width="0.1524" layer="91"/>
<label x="335.28" y="261.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="PORTA-RX" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="PA20/SER5-2/SER3-2/TC7-0"/>
<wire x1="182.88" y1="246.38" x2="208.28" y2="246.38" width="0.1524" layer="91"/>
<label x="185.42" y="246.38" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="RO"/>
<wire x1="347.98" y1="274.32" x2="335.28" y2="274.32" width="0.1524" layer="91"/>
<label x="335.28" y="274.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="RTCXOUT" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="PA01/XOUT32/SER1-1/TC2-1"/>
<wire x1="182.88" y1="294.64" x2="220.98" y2="294.64" width="0.1524" layer="91"/>
<label x="185.42" y="294.64" size="1.778" layer="95"/>
<pinref part="Y2" gate="G$1" pin="1"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="220.98" y1="294.64" x2="228.6" y2="294.64" width="0.1524" layer="91"/>
<junction x="220.98" y="294.64"/>
</segment>
</net>
<net name="RCTXIN" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="PA00/XIN32/SER1-0/TC2-0"/>
<wire x1="182.88" y1="297.18" x2="213.36" y2="297.18" width="0.1524" layer="91"/>
<label x="185.42" y="297.18" size="1.778" layer="95"/>
<pinref part="Y2" gate="G$1" pin="2"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="220.98" y1="299.72" x2="228.6" y2="299.72" width="0.1524" layer="91"/>
<wire x1="213.36" y1="297.18" x2="213.36" y2="299.72" width="0.1524" layer="91"/>
<wire x1="213.36" y1="299.72" x2="220.98" y2="299.72" width="0.1524" layer="91"/>
<junction x="220.98" y="299.72"/>
</segment>
</net>
<net name="VDDCORE" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="VDDCORE"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="104.14" y1="251.46" x2="91.44" y2="251.46" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="91.44" y1="251.46" x2="78.74" y2="251.46" width="0.1524" layer="91"/>
<junction x="91.44" y="251.46"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="81.28" y1="266.7" x2="78.74" y2="266.7" width="0.1524" layer="91"/>
<wire x1="78.74" y1="266.7" x2="78.74" y2="251.46" width="0.1524" layer="91"/>
<junction x="78.74" y="251.46"/>
<label x="81.28" y="251.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="1"/>
<pinref part="U5" gate="G$1" pin="VSW"/>
<wire x1="96.52" y1="266.7" x2="104.14" y2="266.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="NEOPIXEL" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="PB03/ADC0/SER5-1/TC6"/>
<wire x1="182.88" y1="210.82" x2="208.28" y2="210.82" width="0.1524" layer="91"/>
<label x="185.42" y="210.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="ARDUINO13" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="PA23/SER3-1/SER5-0/TC4-1"/>
<wire x1="182.88" y1="238.76" x2="215.9" y2="238.76" width="0.1524" layer="91"/>
<label x="185.42" y="238.76" size="1.778" layer="95"/>
<pinref part="D4" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="C"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="223.52" y1="238.76" x2="231.14" y2="238.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PORTE-TX" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DI"/>
<wire x1="347.98" y1="58.42" x2="335.28" y2="58.42" width="0.1524" layer="91"/>
<label x="335.28" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PB02/ADC0-14/SER5-0/TC6-0"/>
<wire x1="182.88" y1="213.36" x2="208.28" y2="213.36" width="0.1524" layer="91"/>
<label x="185.42" y="213.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="PORTE-RX" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="RO"/>
<wire x1="347.98" y1="71.12" x2="335.28" y2="71.12" width="0.1524" layer="91"/>
<label x="335.28" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PB00/ADC0-12/SER5-2/TC7-0"/>
<wire x1="182.88" y1="218.44" x2="208.28" y2="218.44" width="0.1524" layer="91"/>
<label x="185.42" y="218.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="PORTA-GREEN" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="PB07/ADC1-9"/>
<wire x1="182.88" y1="200.66" x2="208.28" y2="200.66" width="0.1524" layer="91"/>
<label x="185.42" y="200.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="297.18" y1="261.62" x2="274.32" y2="261.62" width="0.1524" layer="91"/>
<label x="274.32" y="261.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="XOUT0" class="0">
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<pinref part="U5" gate="G$1" pin="PA15/XOUT0/SER2-3/SER4-3/TC3-1"/>
<wire x1="182.88" y1="259.08" x2="220.98" y2="259.08" width="0.1524" layer="91"/>
<label x="185.42" y="259.08" size="1.778" layer="95"/>
<pinref part="Y1" gate="G$1" pin="1"/>
<wire x1="220.98" y1="259.08" x2="228.6" y2="259.08" width="0.1524" layer="91"/>
<junction x="220.98" y="259.08"/>
</segment>
</net>
<net name="XIN0" class="0">
<segment>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="215.9" y1="264.16" x2="220.98" y2="264.16" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="PA14/XIN0/SER2-2/SER4-2/TC3-0"/>
<wire x1="220.98" y1="264.16" x2="228.6" y2="264.16" width="0.1524" layer="91"/>
<wire x1="182.88" y1="261.62" x2="215.9" y2="261.62" width="0.1524" layer="91"/>
<wire x1="215.9" y1="261.62" x2="215.9" y2="264.16" width="0.1524" layer="91"/>
<label x="185.42" y="261.62" size="1.778" layer="95"/>
<pinref part="Y1" gate="G$1" pin="2"/>
<junction x="220.98" y="264.16"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="C"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="309.88" y1="261.62" x2="307.34" y2="261.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="D6" gate="G$1" pin="C"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="309.88" y1="274.32" x2="307.34" y2="274.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="D7" gate="G$1" pin="C"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="309.88" y1="58.42" x2="307.34" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="D8" gate="G$1" pin="C"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="309.88" y1="71.12" x2="307.34" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PORTE-GREEN" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="PB05/ADC1-7"/>
<wire x1="182.88" y1="205.74" x2="208.28" y2="205.74" width="0.1524" layer="91"/>
<label x="185.42" y="205.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="297.18" y1="58.42" x2="274.32" y2="58.42" width="0.1524" layer="91"/>
<label x="274.32" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="PORTE-YELLOW" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="PB08/ADC0-2/ADC1-9/SER4-0/TC4-0"/>
<wire x1="182.88" y1="198.12" x2="208.28" y2="198.12" width="0.1524" layer="91"/>
<label x="185.42" y="198.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="297.18" y1="71.12" x2="274.32" y2="71.12" width="0.1524" layer="91"/>
<label x="274.32" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="CRX-A" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="TX-Y"/>
<wire x1="398.78" y1="81.28" x2="403.86" y2="81.28" width="0.1524" layer="91"/>
<wire x1="403.86" y1="81.28" x2="403.86" y2="73.66" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="A"/>
<wire x1="403.86" y1="73.66" x2="383.54" y2="73.66" width="0.1524" layer="91"/>
<label x="386.08" y="73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="CRX-B" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="B"/>
<wire x1="383.54" y1="68.58" x2="408.94" y2="68.58" width="0.1524" layer="91"/>
<wire x1="408.94" y1="68.58" x2="408.94" y2="86.36" width="0.1524" layer="91"/>
<pinref part="U$5" gate="G$1" pin="TX-Z"/>
<wire x1="408.94" y1="86.36" x2="398.78" y2="86.36" width="0.1524" layer="91"/>
<label x="386.08" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="CTX-Z" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="RX-B"/>
<wire x1="398.78" y1="91.44" x2="414.02" y2="91.44" width="0.1524" layer="91"/>
<wire x1="414.02" y1="91.44" x2="414.02" y2="63.5" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="Z"/>
<wire x1="414.02" y1="63.5" x2="383.54" y2="63.5" width="0.1524" layer="91"/>
<label x="386.08" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="CTZ-Y" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="Y"/>
<wire x1="383.54" y1="58.42" x2="419.1" y2="58.42" width="0.1524" layer="91"/>
<wire x1="419.1" y1="58.42" x2="419.1" y2="96.52" width="0.1524" layer="91"/>
<pinref part="U$5" gate="G$1" pin="RX-A"/>
<wire x1="419.1" y1="96.52" x2="398.78" y2="96.52" width="0.1524" layer="91"/>
<label x="386.08" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="PDRX-A" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="TX-Y"/>
<wire x1="398.78" y1="132.08" x2="403.86" y2="132.08" width="0.1524" layer="91"/>
<wire x1="403.86" y1="132.08" x2="403.86" y2="124.46" width="0.1524" layer="91"/>
<pinref part="U$10" gate="G$1" pin="A"/>
<wire x1="403.86" y1="124.46" x2="383.54" y2="124.46" width="0.1524" layer="91"/>
<label x="386.08" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="PDRX-B" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="TX-Z"/>
<wire x1="398.78" y1="137.16" x2="408.94" y2="137.16" width="0.1524" layer="91"/>
<wire x1="408.94" y1="137.16" x2="408.94" y2="119.38" width="0.1524" layer="91"/>
<pinref part="U$10" gate="G$1" pin="B"/>
<wire x1="408.94" y1="119.38" x2="383.54" y2="119.38" width="0.1524" layer="91"/>
<label x="386.08" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="PDTX-Z" class="0">
<segment>
<pinref part="U$10" gate="G$1" pin="Z"/>
<wire x1="383.54" y1="114.3" x2="414.02" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U$11" gate="G$1" pin="RX-B"/>
<wire x1="414.02" y1="114.3" x2="414.02" y2="142.24" width="0.1524" layer="91"/>
<wire x1="414.02" y1="142.24" x2="398.78" y2="142.24" width="0.1524" layer="91"/>
<label x="386.08" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="PDTX-Y" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="RX-A"/>
<wire x1="398.78" y1="147.32" x2="419.1" y2="147.32" width="0.1524" layer="91"/>
<pinref part="U$10" gate="G$1" pin="Y"/>
<wire x1="419.1" y1="147.32" x2="419.1" y2="109.22" width="0.1524" layer="91"/>
<wire x1="419.1" y1="109.22" x2="383.54" y2="109.22" width="0.1524" layer="91"/>
<label x="386.08" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="PORTA-YELLOW" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="PB06/ADC1-8"/>
<wire x1="182.88" y1="203.2" x2="208.28" y2="203.2" width="0.1524" layer="91"/>
<label x="185.42" y="203.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="297.18" y1="274.32" x2="274.32" y2="274.32" width="0.1524" layer="91"/>
<label x="274.32" y="274.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="D9" gate="G$1" pin="C"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="309.88" y1="210.82" x2="307.34" y2="210.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="D10" gate="G$1" pin="C"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="309.88" y1="223.52" x2="307.34" y2="223.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="D11" gate="G$1" pin="C"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="309.88" y1="160.02" x2="307.34" y2="160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="D12" gate="G$1" pin="C"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="309.88" y1="172.72" x2="307.34" y2="172.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="D13" gate="G$1" pin="C"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="309.88" y1="109.22" x2="307.34" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="D14" gate="G$1" pin="C"/>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="309.88" y1="121.92" x2="307.34" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="D15" gate="G$1" pin="C"/>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="309.88" y1="7.62" x2="307.34" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="D16" gate="G$1" pin="C"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="309.88" y1="20.32" x2="307.34" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PORTB-YELLOW" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="297.18" y1="223.52" x2="274.32" y2="223.52" width="0.1524" layer="91"/>
<label x="274.32" y="223.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PB09/ADC0-3/ADC1-1/SER4-1/TC4-1"/>
<wire x1="182.88" y1="195.58" x2="208.28" y2="195.58" width="0.1524" layer="91"/>
<label x="185.42" y="195.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="PORTB-GREEN" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="297.18" y1="210.82" x2="274.32" y2="210.82" width="0.1524" layer="91"/>
<label x="274.32" y="210.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PA04/ANAREF-VREFB/ADC0-4/SER0-0/TC0-0"/>
<wire x1="182.88" y1="287.02" x2="208.28" y2="287.02" width="0.1524" layer="91"/>
<label x="185.42" y="287.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="PORTB-RX" class="0">
<segment>
<pinref part="U$6" gate="G$1" pin="RO"/>
<wire x1="347.98" y1="223.52" x2="335.28" y2="223.52" width="0.1524" layer="91"/>
<label x="335.28" y="223.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PB14/SER4-2/TC5-0"/>
<wire x1="182.88" y1="182.88" x2="208.28" y2="182.88" width="0.1524" layer="91"/>
<label x="185.42" y="182.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="PORTB-TX" class="0">
<segment>
<pinref part="U$6" gate="G$1" pin="DI"/>
<wire x1="347.98" y1="210.82" x2="335.28" y2="210.82" width="0.1524" layer="91"/>
<label x="335.28" y="210.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PB12/SER4-0/TC4-0"/>
<wire x1="182.88" y1="187.96" x2="208.28" y2="187.96" width="0.1524" layer="91"/>
<label x="185.42" y="187.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="PBRX-A" class="0">
<segment>
<pinref part="U$7" gate="G$1" pin="TX-Y"/>
<wire x1="398.78" y1="233.68" x2="403.86" y2="233.68" width="0.1524" layer="91"/>
<wire x1="403.86" y1="233.68" x2="403.86" y2="226.06" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="A"/>
<wire x1="403.86" y1="226.06" x2="383.54" y2="226.06" width="0.1524" layer="91"/>
<label x="386.08" y="226.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="PBRX-B" class="0">
<segment>
<pinref part="U$7" gate="G$1" pin="TX-Z"/>
<wire x1="398.78" y1="238.76" x2="408.94" y2="238.76" width="0.1524" layer="91"/>
<wire x1="408.94" y1="238.76" x2="408.94" y2="220.98" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="B"/>
<wire x1="408.94" y1="220.98" x2="383.54" y2="220.98" width="0.1524" layer="91"/>
<label x="386.08" y="220.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="PBTX-Z" class="0">
<segment>
<pinref part="U$6" gate="G$1" pin="Z"/>
<wire x1="383.54" y1="215.9" x2="414.02" y2="215.9" width="0.1524" layer="91"/>
<pinref part="U$7" gate="G$1" pin="RX-B"/>
<wire x1="414.02" y1="215.9" x2="414.02" y2="243.84" width="0.1524" layer="91"/>
<wire x1="414.02" y1="243.84" x2="398.78" y2="243.84" width="0.1524" layer="91"/>
<label x="386.08" y="215.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="PBTX-Y" class="0">
<segment>
<pinref part="U$7" gate="G$1" pin="RX-A"/>
<wire x1="398.78" y1="248.92" x2="419.1" y2="248.92" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="Y"/>
<wire x1="419.1" y1="248.92" x2="419.1" y2="210.82" width="0.1524" layer="91"/>
<wire x1="419.1" y1="210.82" x2="383.54" y2="210.82" width="0.1524" layer="91"/>
<label x="386.08" y="210.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="PARX-A" class="0">
<segment>
<pinref part="U$4" gate="G$1" pin="TX-Y"/>
<wire x1="398.78" y1="284.48" x2="403.86" y2="284.48" width="0.1524" layer="91"/>
<wire x1="403.86" y1="284.48" x2="403.86" y2="276.86" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="A"/>
<wire x1="403.86" y1="276.86" x2="383.54" y2="276.86" width="0.1524" layer="91"/>
<label x="386.08" y="276.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="PARX-B" class="0">
<segment>
<pinref part="U$4" gate="G$1" pin="TX-Z"/>
<wire x1="398.78" y1="289.56" x2="408.94" y2="289.56" width="0.1524" layer="91"/>
<wire x1="408.94" y1="289.56" x2="408.94" y2="271.78" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="B"/>
<wire x1="408.94" y1="271.78" x2="383.54" y2="271.78" width="0.1524" layer="91"/>
<label x="386.08" y="271.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="PATX-Z" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="Z"/>
<wire x1="383.54" y1="266.7" x2="414.02" y2="266.7" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$1" pin="RX-B"/>
<wire x1="414.02" y1="266.7" x2="414.02" y2="294.64" width="0.1524" layer="91"/>
<wire x1="414.02" y1="294.64" x2="398.78" y2="294.64" width="0.1524" layer="91"/>
<label x="386.08" y="266.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="PATX-Y" class="0">
<segment>
<pinref part="U$4" gate="G$1" pin="RX-A"/>
<wire x1="398.78" y1="299.72" x2="419.1" y2="299.72" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="Y"/>
<wire x1="419.1" y1="299.72" x2="419.1" y2="261.62" width="0.1524" layer="91"/>
<wire x1="419.1" y1="261.62" x2="383.54" y2="261.62" width="0.1524" layer="91"/>
<label x="386.08" y="261.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="PORTC-YELLOW" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="297.18" y1="172.72" x2="274.32" y2="172.72" width="0.1524" layer="91"/>
<label x="274.32" y="172.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PA05/ADC0-5/DAC-1/SER0-1/TC0-1"/>
<wire x1="182.88" y1="284.48" x2="208.28" y2="284.48" width="0.1524" layer="91"/>
<label x="185.42" y="284.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="PORTC-GREEN" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="297.18" y1="160.02" x2="274.32" y2="160.02" width="0.1524" layer="91"/>
<label x="274.32" y="160.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PA06/ANAREF-VREFC/ADC0-6/SER0-2/TC1-0"/>
<wire x1="182.88" y1="281.94" x2="208.28" y2="281.94" width="0.1524" layer="91"/>
<label x="185.42" y="281.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="PORTC-RX" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="RO"/>
<wire x1="347.98" y1="172.72" x2="335.28" y2="172.72" width="0.1524" layer="91"/>
<label x="335.28" y="172.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PA10/ADC0-10/SER0-2/SER2-2/TC1-0/TCC0-2"/>
<wire x1="182.88" y1="271.78" x2="208.28" y2="271.78" width="0.1524" layer="91"/>
<label x="185.42" y="271.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="PORTC-TX" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="DI"/>
<wire x1="347.98" y1="160.02" x2="335.28" y2="160.02" width="0.1524" layer="91"/>
<label x="335.28" y="160.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PA08/ADC0-8/ADC1-2/SER0-0/SER2-1/TC0-0/TCC0-0"/>
<wire x1="182.88" y1="276.86" x2="208.28" y2="276.86" width="0.1524" layer="91"/>
<label x="185.42" y="276.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="PCRX-A" class="0">
<segment>
<pinref part="U$9" gate="G$1" pin="TX-Y"/>
<wire x1="398.78" y1="182.88" x2="403.86" y2="182.88" width="0.1524" layer="91"/>
<wire x1="403.86" y1="182.88" x2="403.86" y2="175.26" width="0.1524" layer="91"/>
<pinref part="U$8" gate="G$1" pin="A"/>
<wire x1="403.86" y1="175.26" x2="383.54" y2="175.26" width="0.1524" layer="91"/>
<label x="386.08" y="175.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="PCRX-B" class="0">
<segment>
<pinref part="U$9" gate="G$1" pin="TX-Z"/>
<wire x1="398.78" y1="187.96" x2="408.94" y2="187.96" width="0.1524" layer="91"/>
<wire x1="408.94" y1="187.96" x2="408.94" y2="170.18" width="0.1524" layer="91"/>
<pinref part="U$8" gate="G$1" pin="B"/>
<wire x1="408.94" y1="170.18" x2="383.54" y2="170.18" width="0.1524" layer="91"/>
<label x="386.08" y="170.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="PCTX-Z" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="Z"/>
<wire x1="383.54" y1="165.1" x2="414.02" y2="165.1" width="0.1524" layer="91"/>
<pinref part="U$9" gate="G$1" pin="RX-B"/>
<wire x1="414.02" y1="165.1" x2="414.02" y2="193.04" width="0.1524" layer="91"/>
<wire x1="414.02" y1="193.04" x2="398.78" y2="193.04" width="0.1524" layer="91"/>
<label x="386.08" y="165.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="PCTX-Y" class="0">
<segment>
<pinref part="U$9" gate="G$1" pin="RX-A"/>
<wire x1="398.78" y1="198.12" x2="419.1" y2="198.12" width="0.1524" layer="91"/>
<pinref part="U$8" gate="G$1" pin="Y"/>
<wire x1="419.1" y1="198.12" x2="419.1" y2="160.02" width="0.1524" layer="91"/>
<wire x1="419.1" y1="160.02" x2="383.54" y2="160.02" width="0.1524" layer="91"/>
<label x="386.08" y="160.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="PORTD-YELLOW" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="297.18" y1="121.92" x2="274.32" y2="121.92" width="0.1524" layer="91"/>
<label x="274.32" y="121.92" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PA03/ANAREF-VREFA/ADC0-1"/>
<wire x1="182.88" y1="289.56" x2="208.28" y2="289.56" width="0.1524" layer="91"/>
<label x="185.42" y="289.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="PORTD-GREEN" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="297.18" y1="109.22" x2="274.32" y2="109.22" width="0.1524" layer="91"/>
<label x="274.32" y="109.22" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PA02/ADC0-1/DAC-0"/>
<wire x1="182.88" y1="292.1" x2="208.28" y2="292.1" width="0.1524" layer="91"/>
<label x="185.42" y="292.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="PORTD-RX" class="0">
<segment>
<pinref part="U$10" gate="G$1" pin="RO"/>
<wire x1="347.98" y1="121.92" x2="335.28" y2="121.92" width="0.1524" layer="91"/>
<label x="335.28" y="121.92" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PA18/SER1-2/SER3-2/TC3-0"/>
<wire x1="182.88" y1="251.46" x2="208.28" y2="251.46" width="0.1524" layer="91"/>
<label x="185.42" y="251.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="PORTD-TX" class="0">
<segment>
<pinref part="U$10" gate="G$1" pin="DI"/>
<wire x1="347.98" y1="109.22" x2="335.28" y2="109.22" width="0.1524" layer="91"/>
<label x="335.28" y="109.22" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PA16/SER1-0/SER3-1/TC2-0/TCC0-4"/>
<wire x1="182.88" y1="256.54" x2="208.28" y2="256.54" width="0.1524" layer="91"/>
<label x="185.42" y="256.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="PORTF-YELLOW" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="297.18" y1="20.32" x2="274.32" y2="20.32" width="0.1524" layer="91"/>
<label x="274.32" y="20.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PA11/ADC0-11/SER0-3/SER2-3/TC1-1/TCC0-3"/>
<wire x1="182.88" y1="269.24" x2="208.28" y2="269.24" width="0.1524" layer="91"/>
<label x="185.42" y="269.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="PORTF-GREEN" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="297.18" y1="7.62" x2="274.32" y2="7.62" width="0.1524" layer="91"/>
<label x="274.32" y="7.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PA09/ADC0-9/ADC1-3/SER0-1/SER2-0/TC0-1/TCC0-1"/>
<wire x1="182.88" y1="274.32" x2="208.28" y2="274.32" width="0.1524" layer="91"/>
<label x="185.42" y="274.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="CFRX-A" class="0">
<segment>
<pinref part="U$13" gate="G$1" pin="TX-Y"/>
<wire x1="398.78" y1="30.48" x2="403.86" y2="30.48" width="0.1524" layer="91"/>
<wire x1="403.86" y1="30.48" x2="403.86" y2="22.86" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="A"/>
<wire x1="403.86" y1="22.86" x2="383.54" y2="22.86" width="0.1524" layer="91"/>
<label x="386.08" y="22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="CFRX-B" class="0">
<segment>
<pinref part="U$12" gate="G$1" pin="B"/>
<wire x1="383.54" y1="17.78" x2="408.94" y2="17.78" width="0.1524" layer="91"/>
<wire x1="408.94" y1="17.78" x2="408.94" y2="35.56" width="0.1524" layer="91"/>
<pinref part="U$13" gate="G$1" pin="TX-Z"/>
<wire x1="408.94" y1="35.56" x2="398.78" y2="35.56" width="0.1524" layer="91"/>
<label x="386.08" y="17.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="CFTX-Z" class="0">
<segment>
<pinref part="U$13" gate="G$1" pin="RX-B"/>
<wire x1="398.78" y1="40.64" x2="414.02" y2="40.64" width="0.1524" layer="91"/>
<wire x1="414.02" y1="40.64" x2="414.02" y2="12.7" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="Z"/>
<wire x1="414.02" y1="12.7" x2="383.54" y2="12.7" width="0.1524" layer="91"/>
<label x="386.08" y="12.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="CFTZ-Y" class="0">
<segment>
<pinref part="U$12" gate="G$1" pin="Y"/>
<wire x1="383.54" y1="7.62" x2="419.1" y2="7.62" width="0.1524" layer="91"/>
<wire x1="419.1" y1="7.62" x2="419.1" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U$13" gate="G$1" pin="RX-A"/>
<wire x1="419.1" y1="45.72" x2="398.78" y2="45.72" width="0.1524" layer="91"/>
<label x="386.08" y="7.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="PORTF-RX" class="0">
<segment>
<pinref part="U$12" gate="G$1" pin="RO"/>
<wire x1="347.98" y1="20.32" x2="335.28" y2="20.32" width="0.1524" layer="91"/>
<label x="335.28" y="20.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PA13/SER2-1/SER4-0/TC2-1/TCC0-7"/>
<wire x1="182.88" y1="264.16" x2="208.28" y2="264.16" width="0.1524" layer="91"/>
<label x="185.42" y="264.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="PORTF-TX" class="0">
<segment>
<pinref part="U$12" gate="G$1" pin="DI"/>
<wire x1="347.98" y1="7.62" x2="335.28" y2="7.62" width="0.1524" layer="91"/>
<label x="335.28" y="7.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="PA12/SER2-0/SER4-1/TC2-0/TCC0-6"/>
<wire x1="182.88" y1="266.7" x2="208.28" y2="266.7" width="0.1524" layer="91"/>
<label x="185.42" y="266.7" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
