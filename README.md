# Squidworks Embedded Router

Message passing hardware for [squidworks](https://gitlab.cba.mit.edu/squidworks/squidworks) networks.

![board](routerboard-atsamd51/2019-11-04_router.jpg)

![routed](routerboard-atsamd51/routed.png)
![schem](routerboard-atsamd51/schematic.png)
